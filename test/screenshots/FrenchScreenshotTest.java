/*
 * FrenchScreenshotTest.java
 *
 * Created on 11. Februar 2006, 08:41
 *
 */
package screenshots;

import java.util.Locale;
import junit.framework.TestCase;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class FrenchScreenshotTest extends TestCase {

    public void testDoScreenShots() throws Exception {

        assertTrue("Screenshots are not generated!", Screenshots.UPDATE_SCREENSHOTS);

        Screenshots.doScreenShots(Locale.FRENCH, "doc/docbook/fr/",
                380, 489, "MainWindow",
                "petit poid", "peas", "poids (legume)", "NewCardDialog",
                "test/testLessons/Currencies.xml.gz", 550, 489, "Learning",
                "FontDialog",
                "Strategies", "Times", "Misc",
                "test/testLessons/Currencies.xml.gz", "UltraShortermMemory");
    }
}
