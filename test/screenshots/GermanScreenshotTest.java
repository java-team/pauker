/*
 * GermanScreenshotTest.java
 *
 * Created on 29. Oktober 2006, 08:41
 *
 */
package screenshots;

import java.util.Locale;
import junit.framework.TestCase;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class GermanScreenshotTest extends TestCase {

    public void testDoScreenShots() throws Exception {

        assertTrue("Screenshots are not generated!", Screenshots.UPDATE_SCREENSHOTS);

        Screenshots.doScreenShots(Locale.GERMAN, "doc/docbook/de/",
                400, 488, "Hauptfenster",
                "weniger ist manchmal...", "mehr", "zu wenig", "NeueKarte",
                "test/testLessons/Laender-Hauptstaedte.xml.gz", 487, 489, "Lernen",
                "SchriftartDialog",
                "Strategien", "Zeiten", "Sonstiges",
                "test/testLessons/Laender-Hauptstaedte.xml.gz", "UltraKurzzeitGedaechtnis");
    }
}
