/*
 * Screenshots.java
 *
 * Created on 29. Oktober 2006, 08:41
 *
 */
package screenshots;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFileChooserOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import org.netbeans.jemmy.util.PNGEncoder;
import pauker.program.Card;
import pauker.program.gui.swing.FontDialog;
import pauker.program.gui.swing.PaukerFrame;
import pauker.program.gui.swing.SettingsDialog;
import pauker.program.gui.swing.Tools;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public final class Screenshots {

    /**
     * if true, the unit test just create screenshots
     */
    public static final boolean UPDATE_SCREENSHOTS = false;
    
    private Screenshots() {
        // singleton
    }

    /**
     * creates all screenshots
     * @param locale the locale to use
     * @param path the path where to store the screenshots
     * @param mainWindowWidth the width of the main window to be used
     * @param mainWindowHeight the height of the main window to be used
     * @param mainWindowFileName the file name of the main window screen shot
     * @param badFrontsideString the file name of the bad front side screen shot
     * @param badReversesideString the file name of the bad reverse side screen
     * shot
     * @param frontsideString the string to be used for the front side
     * @param newCardDialogFileName the file name of the new card dialog screen
     * shot
     * @param learningLesson the lesson for the learning screen shot
     * @param learningWidth the width of the learning screenshot window
     * @param learningHeight the height of the learning screenshot window
     * @param learningFileName the file name for the learning screenshot
     * @param fontDialogFileName the file name for the font dialog screenshot
     * @param strategiesFileName the file name for the strategies screenshot
     * @param timesFileName the file name for the times screenshot
     * @param miscFileName the file name for the misc screenshot
     * @param ultraShortTermLesson the lesson to be used for the ultra short
     * term memory screenshot
     * @param ultraShortTermFileName the file name for the ultra short term
     * screenshot
     * @throws Exception if an exception occurs
     */
    public static void doScreenShots(Locale locale, String path,
            int mainWindowWidth, int mainWindowHeight, String mainWindowFileName,
            String badFrontsideString, String badReversesideString, String frontsideString, String newCardDialogFileName,
            String learningLesson, int learningWidth, int learningHeight, String learningFileName,
            String fontDialogFileName,
            String strategiesFileName, String timesFileName, String miscFileName,
            String ultraShortTermLesson, String ultraShortTermFileName)
            throws Exception {

        // make sure that path exists
        File directory = new File(path);
        if ((!directory.exists()) && (!directory.mkdir())) {
            throw new IOException("Could not create directory \"" + path + "\"");
        }

        Locale.setDefault(locale);
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                frameOperator, new NameComponentChooser("splitPane"));
        final JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JMenuOperator fileMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("fileMenu"));
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();

        /**
         * settings dialog screenshots
         */
        if ((strategiesFileName != null) ||
                (timesFileName != null) ||
                (miscFileName != null)) {
            final SettingsDialog settingsDialog = new SettingsDialog(paukerFrame);
            Thread thread = new Thread() {

                @Override
                public void run() {
                    // this is a blocking call, that's why it must be run in a thread
                    settingsDialog.setVisible(true);
                }
            };
            thread.start();
            JDialogOperator dialogOperator = new JDialogOperator(settingsDialog);
            JListOperator menuListOperator = new JListOperator(
                    dialogOperator, new NameComponentChooser("menuList"));
            if (strategiesFileName != null) {
                PNGEncoder.captureScreen(dialogOperator.getBounds(),
                        path + strategiesFileName + ".png", PNGEncoder.COLOR_MODE);
            }
            if (timesFileName != null) {
                menuListOperator.setSelectedIndex(1);
                PNGEncoder.captureScreen(dialogOperator.getBounds(),
                        path + timesFileName + ".png", PNGEncoder.COLOR_MODE);
            }
            if (miscFileName != null) {
                menuListOperator.setSelectedIndex(2);
                PNGEncoder.captureScreen(dialogOperator.getBounds(),
                        path + miscFileName + ".png", PNGEncoder.COLOR_MODE);
            }
            dialogOperator.setVisible(false);
        }

        /**
         * main window screenshot
         */
        // offset for KDE decorations
        int decorationOffsetX = 8;
        int decorationOffsetY = 27;
        splitPaneOperator.setDividerLocation(splitPaneOperator.getMinimumDividerLocation());
        frameOperator.resize(mainWindowWidth + decorationOffsetX, mainWindowHeight + decorationOffsetY);
        Thread.sleep(1000);
        if (mainWindowFileName != null) {
            PNGEncoder.captureScreen(frameOperator.getBounds(),
                    path + mainWindowFileName + ".png", PNGEncoder.COLOR_MODE);
        }

        /**
         * card dialog screenshot
         */
        if (newCardDialogFileName != null) {
            // fill in bad example card
            addCardButtonOperator.pushNoBlock();
            JDialogOperator dialogOperator = new JDialogOperator();
            JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                    dialogOperator, new NameComponentChooser("frontSideTextArea"));
            JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                    dialogOperator, new NameComponentChooser("reverseSideTextArea"));
            JButtonOperator okButtonOperator = new JButtonOperator(
                    dialogOperator, new NameComponentChooser("okButton"));

            frontSideTextAreaOperator.setText(badFrontsideString);
            frontSideTextAreaOperator.setForeground(Color.RED);
            frontSideTextAreaOperator.setBackground(Color.BLUE);
            frontSideTextAreaOperator.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 19));
            reverseSideTextAreaOperator.setText(badReversesideString);
            reverseSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 19));
            Tools.doClick(okButtonOperator);

            // start to fill another card
            addCardButtonOperator.pushNoBlock();
            dialogOperator = new JDialogOperator();
            frontSideTextAreaOperator = new JTextAreaOperator(
                    dialogOperator, new NameComponentChooser("frontSideTextArea"));
            reverseSideTextAreaOperator = new JTextAreaOperator(
                    dialogOperator, new NameComponentChooser("reverseSideTextArea"));
            JButtonOperator cancelButtonOperator = new JButtonOperator(
                    dialogOperator, new NameComponentChooser("cancelButton"));
            frontSideTextAreaOperator.setText(frontsideString);
            frontSideTextAreaOperator.setForeground(Color.BLACK);
            frontSideTextAreaOperator.setBackground(Color.WHITE);
            frontSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 12));
            reverseSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 12));
            Thread.sleep(1000);
            PNGEncoder.captureScreen(dialogOperator.getBounds(),
                    path + newCardDialogFileName + ".png", PNGEncoder.COLOR_MODE);
            Tools.doClick(cancelButtonOperator);
        }

        /**
         * learning screenshot
         */
        if (learningFileName != null) {
            fileMenuOperator.doClick();
            JMenuItemOperator openMenuItemOperator = new JMenuItemOperator(
                    frameOperator, new NameComponentChooser("openMenuItem"));
            openMenuItemOperator.push();
            JDialogOperator questionDialogOperator = new JDialogOperator();
            JButtonOperator dontSaveButtonOperator = new JButtonOperator(questionDialogOperator, 1);
            Tools.doClick(dontSaveButtonOperator);
            JDialogOperator openFileDialogOperator = new JDialogOperator();
            JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
            fileChooserOperator.setSelectedFile(new File(learningLesson));
            JButtonOperator openButtonOperator = new JButtonOperator(
                    openFileDialogOperator, new NameComponentChooser("openButton"));
            Tools.doClick(openButtonOperator);
            frameOperator.resize(learningWidth + decorationOffsetX, learningHeight + decorationOffsetY);
            Thread.sleep(1000);
            PNGEncoder.captureScreen(frameOperator.getBounds(),
                    path + learningFileName + ".png", PNGEncoder.COLOR_MODE);
        }

        /**
         * font dialog screenshot
         */
        if (fontDialogFileName != null) {
            final FontDialog fontDialog = new FontDialog(paukerFrame,
                    new Font("Dialog", Font.PLAIN, 13), Color.BLACK,
                    Color.WHITE, Card.Element.FRONT_SIDE);
            Thread thread = new Thread() {

                @Override
                public void run() {
                    // this is a blocking call, that's why it must be run in a thread
                    fontDialog.setVisible(true);
                }
            };
            thread.start();
            JDialogOperator dialogOperator = new JDialogOperator();
            PNGEncoder.captureScreen(dialogOperator.getBounds(),
                    path + fontDialogFileName + ".png", PNGEncoder.COLOR_MODE);
            dialogOperator.setVisible(false);
        }

        /**
         * ultra-shortterm memory screenshot
         */
        if (ultraShortTermFileName != null) {
            fileMenuOperator.doClick();
            JMenuItemOperator openMenuItemOperator = new JMenuItemOperator(
                    frameOperator, new NameComponentChooser("openMenuItem"));
            openMenuItemOperator.push();
            JDialogOperator questionDialogOperator = new JDialogOperator();
            JButtonOperator dontSaveButtonOperator = new JButtonOperator(questionDialogOperator, 1);
            Tools.doClick(dontSaveButtonOperator);
            JDialogOperator openFileDialogOperator = new JDialogOperator();
            JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
            fileChooserOperator.setSelectedFile(new File(ultraShortTermLesson));
            JButtonOperator openButtonOperator = new JButtonOperator(
                    openFileDialogOperator, new NameComponentChooser("openButton"));
            Tools.doClick(openButtonOperator);
            JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                    frameOperator, new NameComponentChooser("learnNewCardsButton"));
            JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                    frameOperator, new NameComponentChooser("nextNewCardButton"));
            splitPaneOperator.setDividerLocation(splitPaneOperator.getMaximumDividerLocation());
            Tools.doClick(learnNewCardsButtonOperator);
            for (int i = 0; i < 10; i++) {
                Tools.doClick(nextNewCardButtonOperator);
            }
            Thread.sleep(10001);
            PNGEncoder.captureScreen(frameOperator.getBounds(),
                    path + ultraShortTermFileName + ".png", PNGEncoder.COLOR_MODE);
        }

        // finish
        frameOperator.setVisible(false);
    }
}
