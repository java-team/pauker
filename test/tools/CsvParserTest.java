/*
 * CsvParserTest.java
 * JUnit based test
 *
 * Created on 12. April 2006, 15:55
 */
package tools;

import java.io.StringReader;
import java.util.List;
import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 *
 * @author standtke@swiss-it.ch
 */
public class CsvParserTest extends TestCase {

    /**
     * Test of parseCsvFile method, of class tools.CsvParser.
     * @throws Exception if any exception occurs
     */
    public void testParseCsvFile() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // *** first simple test ***
        // create test data
        String csvTestString =
                "\"www.swiss-it.ch\", 42 , ,\"Sonnabend, April 23, 2005\", \"Ronny \"\"Pony\"\"\"";
        StringReader reader = new StringReader(csvTestString);

        // parse CSV
        List<List<String>> csvFile = CsvParser.parseCsvFile(reader);
        List<String> csvLine = csvFile.get(0);

        // test results
        assertEquals("www.swiss-it.ch", csvLine.get(0));
        assertEquals("42", csvLine.get(1));
        assertEquals(null, csvLine.get(2));
        assertEquals("Sonnabend, April 23, 2005", csvLine.get(3));
        assertEquals("Ronny \"Pony\"", csvLine.get(4));

        // *** line break test ***
        // create test data
        csvTestString =
                "\"Zeilen\numbruch UNIX\",\"Zeilen\rumbruch Mac\",\"Zeilen\r\numbruch Win\"\n" +
                "UNIX\n" +
                "Mac\r" +
                "Win\r\n" +
                "Ende";
        reader = new StringReader(csvTestString);

        // parse CSV
        csvFile = CsvParser.parseCsvFile(reader);

        // test results
        assertEquals(5, csvFile.size());
        csvLine = csvFile.get(0);
        assertEquals("Zeilen\numbruch UNIX", csvLine.get(0));
        assertEquals("Zeilen\numbruch Mac", csvLine.get(1));
        assertEquals("Zeilen\numbruch Win", csvLine.get(2));
        csvLine = csvFile.get(1);
        assertEquals("UNIX", csvLine.get(0));
        csvLine = csvFile.get(2);
        assertEquals("Mac", csvLine.get(0));
        csvLine = csvFile.get(3);
        assertEquals("Win", csvLine.get(0));
        csvLine = csvFile.get(4);
        assertEquals("Ende", csvLine.get(0));

        // *** test newline before EOF ***
        csvTestString = "Zeilenumbruch\n";
        reader = new StringReader(csvTestString);

        // parse CSV
        csvFile = CsvParser.parseCsvFile(reader);
        csvLine = csvFile.get(0);

        // test results
        assertEquals(1, csvLine.size());
        assertEquals("Zeilenumbruch", csvLine.get(0));

        // *** missing comma test ***
        csvTestString = "\"Fehler\" behadlung";
        reader = new StringReader(csvTestString);

        // parse CSV
        try {
            csvFile = CsvParser.parseCsvFile(reader);
            fail("this MUST throw an Exception");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // *** missing terminating double quote test ***
        csvTestString = "\"keine Quoten mehr";
        reader = new StringReader(csvTestString);

        // parse CSV
        try {
            csvFile = CsvParser.parseCsvFile(reader);
            fail("this MUST throw an Exception");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // *** tolerate missing quotes ***
        csvTestString =
                "dzień dobry,guten Tag\r\n" +
                "dzień,Tag";
        reader = new StringReader(csvTestString);
        csvFile = CsvParser.parseCsvFile(reader);
        
        // deal with UFT-8 Byte Order Marks
        // see http://de.wikipedia.org/wiki/Byte_Order_Mark
        // and http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4508058
        csvTestString = "\uFEFF\"gehen,laufen\",\"go\n" +
                "went\n" +
                "gone\"\n" +
                "\"sehen\",\"see,\n" +
                "saw,\n" +
                "saw\"";
        reader = new StringReader(csvTestString);
        csvFile = CsvParser.parseCsvFile(reader);
        csvLine = csvFile.get(0);
        assertEquals("gehen,laufen", csvLine.get(0));
    }
}
