/*
 * LessonTest.java
 * JUnit based test
 *
 * Created on 25. August 2007, 18:58
 */

package pauker.program;

import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 * tests the functions of the Lesson class
 * @author Ronny.Standtke@gmx.net
 */
public class LessonTest extends TestCase {
    
    private static CardSide cardSide;
    private static Card card1;
    
    static {
        cardSide = new CardSide();
        card1 = new Card(cardSide, cardSide);
    }
    
    /**
     * tests that the getNextExpirationTime() works as expected
     */
    public void testGetNextExpirationTime() {
        assertFalse("Only screenshots are generated!", 
                Screenshots.UPDATE_SCREENSHOTS);
        
        Lesson lesson = new Lesson();
        Batch firstLongTermBatch = lesson.addLongTermBatch();
        card1.setLearned(true);
        firstLongTermBatch.addCard(card1);
        
        Batch secondLongTermBatch = lesson.addLongTermBatch();
        Card card2 = new Card(cardSide, cardSide);
        card2.setLearned(true);
        secondLongTermBatch.addCard(card2);
        
        assertEquals(
                "next expiration time is not the time from the first card!",
                card1.getExpirationTime(), lesson.getNextExpirationTime());
    }
    
    /**
     * tests that shuffling has similar performance in both the unlearned and
     * the long term batch
     */
    public void testShuffling() {
        assertFalse("Only screenshots are generated!", 
                Screenshots.UPDATE_SCREENSHOTS);
        
        System.out.println("\ntest \"shuffling\" cards");
        
        // preparation
        final int NUMBER_OF_CARDS = 20000;
        Lesson lesson = new Lesson();
        Batch firstLongTermBatch = lesson.addLongTermBatch();
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            lesson.addCard(card1);
        }
        int[] emptySelection = new int[0];
        
        // speed measurememt in unlearned batch
        long start = System.currentTimeMillis();
        lesson.getUnlearnedBatch().shuffle(emptySelection);
        long stop = System.currentTimeMillis();
        long timeInUnlearnedBatch = stop - start;
        System.out.println(
                "timeInUnlearnedBatch = " + timeInUnlearnedBatch + " ms");
        
        // expire all cards
        int[] selectedIndices = new int[NUMBER_OF_CARDS];
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            selectedIndices[i] = i;
        }
        lesson.instantRepeatCards(lesson.getUnlearnedBatch(), selectedIndices);
        
        // speed measurememt in first long term batch
        firstLongTermBatch = lesson.getLongTermBatch(0);
        start = System.currentTimeMillis();
        firstLongTermBatch.shuffle(emptySelection);
        stop = System.currentTimeMillis();
        long timeInFirstLongTermBatch = stop - start;
        System.out.println("timeInFirstLongTermBatch = " + 
                timeInFirstLongTermBatch + " ms");
        
        // check that the times do not differ very much
        long max = Math.max(timeInUnlearnedBatch, timeInFirstLongTermBatch);
        long min = Math.min(timeInUnlearnedBatch, timeInFirstLongTermBatch);
        
        assertTrue("times in the unlearned batch (" + timeInUnlearnedBatch +
                "ms) and in the first long term batch (" +
                timeInFirstLongTermBatch + " ms) differ substantially!",
                max / min < 3);
        
    }
    
    /**
     * tests that calling instantRepeatCards() has similar performance on both
     * the unlearned and the summary batch
     */
    public void testInstantRepeatCards() {
        assertFalse("Only screenshots are generated!", 
                Screenshots.UPDATE_SCREENSHOTS);
        
        System.out.println("\ntest \"instant repeating\" cards");
        
        // preparation
        final int NUMBER_OF_CARDS = 20000;
        Lesson lesson = new Lesson();
        Batch firstLongTermBatch = lesson.addLongTermBatch();
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            CardSide tmpCardSide = new CardSide(String.valueOf(i));
            Card card = new Card(tmpCardSide, tmpCardSide);
            lesson.addCard(card);
        }
        // "select" all cards
        int[] selectedIndices = new int[NUMBER_OF_CARDS];
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            selectedIndices[i] = i;
        }
        
        // measure time when working on the unlearned batch
        long start = System.currentTimeMillis();
        lesson.instantRepeatCards(lesson.getUnlearnedBatch(), selectedIndices);
        long stop = System.currentTimeMillis();
        long timeInUnlearnedBatch = stop - start;
        System.out.println(
                "timeInUnlearnedBatch = " + timeInUnlearnedBatch + " ms");
        
        // create worst case (every card is in long term batch 2)
        Batch secondLongTermBatch = lesson.addLongTermBatch();
        for (int i = firstLongTermBatch.getNumberOfCards() - 1; i >= 0; i--) {
            Card card = firstLongTermBatch.removeCard(i);
            card.setLearned(true);
            secondLongTermBatch.addCard(card);
        }
        
        // measure time when working on the summary batch
        start = System.currentTimeMillis();
        lesson.instantRepeatCards(lesson.getSummaryBatch(), selectedIndices);
        stop = System.currentTimeMillis();
        long timeInSummaryBatch = stop - start;
        System.out.println(
                "timeInSummaryBatch = " + timeInSummaryBatch + " ms");

        // check that the times do not differ very much
        long max = Math.max(timeInUnlearnedBatch, timeInSummaryBatch);
        long min = Math.min(timeInUnlearnedBatch, timeInSummaryBatch);
        
        assertTrue("times in the unlearned batch (" + timeInUnlearnedBatch +
                "ms) and in the summary batch (" + timeInSummaryBatch + 
                " ms) differ substantially!", max / min < 3);
    }
    
    /**
     * tests that putting cards back to the unlearned batch has similar
     * performance on both the first long term and the summary batch
     */
    public void testForgetting() {
        assertFalse("Only screenshots are generated!", 
                Screenshots.UPDATE_SCREENSHOTS);
        
        System.out.println("\ntest \"forgetting\" cards");
        
        // prepare a lesson with lots of cards in the first long term batch
        final int NUMBER_OF_CARDS = 20000;
        Lesson lesson = new Lesson();
        Batch firstLongTermBatch = lesson.addLongTermBatch();
        SummaryBatch summaryBatch = lesson.getSummaryBatch();
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            CardSide tmpCardSide = new CardSide(String.valueOf(i));
            Card card = new Card(tmpCardSide, tmpCardSide);
            card.setLearned(true);
            firstLongTermBatch.addCard(card);
            summaryBatch.addCard(card);
        }
        // "select" all cards
        int[] selectedIndices = new int[NUMBER_OF_CARDS];
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            selectedIndices[i] = i;
        }
        
        // measure time when working on the first long term batch
        long start = System.currentTimeMillis();
        lesson.forgetCards(lesson.getLongTermBatch(0), selectedIndices);
        long stop = System.currentTimeMillis();
        long timeInLongTermBatch = stop - start;
        System.out.println(
                "timeInLongTermBatch = " + timeInLongTermBatch + " ms");
        
        // create worst case for summary operation
        // (every card is in long term batch)
        Batch unlearnedBatch = lesson.getUnlearnedBatch();
        firstLongTermBatch = lesson.addLongTermBatch();
        for (int i = unlearnedBatch.getNumberOfCards() - 1; i >= 0; i--) {
            Card card = unlearnedBatch.removeCard(i);
            card.setLearned(true);
            firstLongTermBatch.addCard(card);
        }
        
        // measure time when working on the summary batch
        start = System.currentTimeMillis();
        lesson.forgetCards(lesson.getSummaryBatch(), selectedIndices);
        stop = System.currentTimeMillis();
        long timeInSummaryBatch = stop - start;
        System.out.println(
                "timeInSummaryBatch = " + timeInSummaryBatch + " ms");

        // check that the times do not differ very much
        long max = Math.max(timeInLongTermBatch, timeInSummaryBatch);
        long min = Math.min(timeInLongTermBatch, timeInSummaryBatch);
        
        assertTrue("times in the unlearned batch (" + timeInLongTermBatch +
                "ms) and in the summary batch (" + timeInSummaryBatch +
                " ms) differ substantially!", max / min < 4);
    }
}
