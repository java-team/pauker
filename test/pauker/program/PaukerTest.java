/*
 * PaukerTest.java
 * JUnit based test
 *
 * Created on 13. April 2006, 16:13
 */

package pauker.program;

import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class PaukerTest extends TestCase {

    /**
     * Test of lessonToCSV method, of class pauker.program.Pauker.
     */
    public void testLessonToCSV() {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // simple test
        Lesson lesson = new Lesson();
        addCard(lesson, "1", "2");
        String expResult = "1,2";
        String result = Pauker.lessonToCSV(lesson);
        assertEquals(expResult, result);

        // test newline insertion
        lesson = new Lesson();
        addCard(lesson, "1", "2");
        addCard(lesson, "3", "4");
        expResult =
                "1,2\n" +
                "3,4";
        result = Pauker.lessonToCSV(lesson);
        assertEquals(expResult, result);

        // test all four escape situations
        lesson = new Lesson();
        addCard(lesson, "kom,ma", " leer zeichen ");
        addCard(lesson, "hoch\"komma", "Zeilen\numbruch");
        expResult =
                "\"kom,ma\",\" leer zeichen \"\n" +
                "\"hoch\"\"komma\",\"Zeilen\numbruch\"";
        result = Pauker.lessonToCSV(lesson);
        assertEquals(expResult, result);
    }

    private void addCard(Lesson lesson, String frontText, String reverseText) {
        CardSide frontSide = new CardSide(frontText);
        CardSide reverseSide = new CardSide(reverseText);
        Card card = new Card(frontSide, reverseSide);
        lesson.addCard(card);
    }
}
