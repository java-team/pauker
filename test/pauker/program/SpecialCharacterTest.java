/*
 * SpecialCharacterTest.java
 *
 * Created on 20.09.2008, 13:05:10
 *
 */

package pauker.program;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 * A test that verifies that Pauker can handle special characters. For details
 * see http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6371411
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class SpecialCharacterTest extends TestCase {

    /**
     * runs the test
     * @throws Exception if an exception occurs
     */
    public void testSpecialCharacters() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // show some logs on the console
        Logger logger = Logger.getLogger(Pauker.class.getName());
        logger.setLevel(Level.FINEST);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.FINEST);
        logger.addHandler(consoleHandler);

        // create card with some special characters
        Lesson lesson = new Lesson();
        String frontSideText = "Via copy & paste you can insert " +
                "\"\u0001\" into the front side.";
        CardSide frontSide = new CardSide(frontSideText);
        String reverseSideText = "Via copy & paste you can insert " +
                "\"\u0001\" into the reverse side.";
        CardSide reverseSide = new CardSide(reverseSideText);
        Card card = new Card(frontSide, reverseSide);
        lesson.addCard(card);

        // create XML
        String xmlString = Pauker.lessonToXML(lesson);
        System.out.println("xmlString:\n" + xmlString);

        // save to temporary file
        File tmpFile = File.createTempFile("testLesson", ".pau.gz");
        FileOutputStream fileOutputStream = null;
        GZIPOutputStream gzipOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(tmpFile);
            gzipOutputStream = new GZIPOutputStream(fileOutputStream);
            gzipOutputStream.write(xmlString.getBytes("UTF-8"));
            gzipOutputStream.finish();
            gzipOutputStream.flush();
            fileOutputStream.flush();
        } finally {
            gzipOutputStream.close();
            fileOutputStream.close();
        }
        // try opening the lesson again
        Lesson parsedLesson = Pauker.openLesson(tmpFile.getPath(), null);

        // check parsed lesson
        Batch unlearnedBatch = parsedLesson.getUnlearnedBatch();
        assertEquals(1, unlearnedBatch.getNumberOfCards());
        Card parsedCard = unlearnedBatch.getCard(0);
        String parsedFrontSideText = parsedCard.getFrontSide().getText();
        String parsedReverseSideText = parsedCard.getReverseSide().getText();

        assertEquals(Pauker.filter(frontSideText), parsedFrontSideText);
        assertEquals(Pauker.filter(reverseSideText), parsedReverseSideText);
    }
}
