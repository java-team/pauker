/*
 * FontChangeBackgroundTest.java
 *
 * Created on 02.02.2008, 17:22:25
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pauker.program.gui.swing;

import java.awt.Color;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JRadioButtonOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

public class FontChangeBackgroundTest extends TestCase {

    /**
     * there was a bug that when all cards had a "null" background
     * the font dialog set the background also to "null" which resulted in a
     * gray background. this looked horrible and was inconsistent.
     * 
     * @throws java.lang.Exception
     */
    public void testFontChangeBackground() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // get references to test lessons
        String internalPath = "/testLessons/Test.csv";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator fontButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("fontButton"));

        // load summary
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);

        // select all cards
        int size = batchListOperator.getModel().getSize();
        batchListOperator.setSelectionInterval(0, size - 1);

        // open font dialog
        fontButtonOperator.pushNoBlock();
        JDialogOperator fontDialogOperator = new JDialogOperator();
        JTextAreaOperator textAreaOperator = new JTextAreaOperator(
                fontDialogOperator, new NameComponentChooser("textArea"));
        JRadioButtonOperator bothRadioButtonOperator = new JRadioButtonOperator(
                fontDialogOperator, new NameComponentChooser("bothRadioButton"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                fontDialogOperator, new NameComponentChooser("okButton"));
        
        // check background color
        Color background = textAreaOperator.getBackground();
        assertEquals("Background must stay white, even when all cards have a " +
                "\"null\" background!", Color.WHITE, background);
        
        // another bug was that the selected side was never saved
        bothRadioButtonOperator.setSelected(true);
        okButtonOperator.doClick();
        fontButtonOperator.pushNoBlock();
        fontDialogOperator = new JDialogOperator();
        bothRadioButtonOperator = new JRadioButtonOperator(
                fontDialogOperator, new NameComponentChooser("bothRadioButton"));
        assertTrue("Side selection was not saved and restored!",
                bothRadioButtonOperator.isSelected());
    }
}
