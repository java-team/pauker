/*
 * SimilarListTest.java
 *
 * Created on 29. Januar 2007, 15:51
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * testing the list of similar cards
 * @author Ronny.Standtke@gmx.net
 */
public class SimilarListTest extends TestCase {

    public void testSimilarList() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // get reference to test file
        String internalPath = "/testLessons/Laender-Hauptstaedte.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        // start pauker with test file
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        final JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator saveButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("saveButton"));

        // try inserting a new card
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("frontSideTextArea"));
        final JListOperator similarCardsListOperator = new JListOperator(
                newDialogOperator, new NameComponentChooser("similarCardsList"));
        newFrontSideTextAreaOperator.setText("Schweiz");
        // give similar cards list some time to fill up
        Thread.sleep(1000);

        // double click similar cards list to open the edit dialog
        Thread thread = new Thread() {

            @Override
            public void run() {
                // this is a blocking call, that's why it must be run in a thread
                similarCardsListOperator.clickOnItem(0, 2);
            }
        };
        thread.start();

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");
        JDialogOperator editDialogOperator = new JDialogOperator(strings.getString("Edit_Card"));
        JTextAreaOperator editFrontSideTextAreaOperator = new JTextAreaOperator(
                editDialogOperator, new NameComponentChooser("frontSideTextArea"));
        JButtonOperator editOKButtonOperator = new JButtonOperator(
                editDialogOperator, new NameComponentChooser("editOKButton"));
        editFrontSideTextAreaOperator.setText("Test");
        Tools.doClick(editOKButtonOperator);

        // something has changed, the save button must become enabled
        saveButtonOperator.waitComponentEnabled();
    }
}
