/*
 * FlipTest.java
 *
 * Created on 10. Juni 2007, 10:28
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComponentOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * Tests that searching for similar cards works after flipping a lesson
 * (there was the following bug: similar cards where searched at the wrong card
 * side after flipping a lesson)
 *
 * @author Ronny.Standtke@gmx.net
 */
public class FlipTest extends TestCase {

    /**
     * tests the flip method
     * @throws java.lang.Exception if an exception occurs
     */
    public void testFlip() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get path of test lesson
        String internalPath = "/testLessons/Laender-Hauptstaedte.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JMenuOperator fileMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("fileMenu"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JComponentOperator aboutPanelOperator = new JComponentOperator(
                frameOperator, new NameComponentChooser("aboutPanel"));
        
        // flip the lesson
        fileMenuOperator.doClick();
        JMenuItemOperator flipCardSidesMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("flipCardSidesMenuItem"));
        flipCardSidesMenuItemOperator.push();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator flipButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Flip_Card_Sides"));
        Tools.doClick(flipButtonOperator);

        // test, if the lesson description is still visible after flipping
        // (loading the summary batch after flipping the lesson was a waste
        // and not consistent with loading a lesson)
        Thread.sleep(1000);
        assertTrue("batch was loaded after flipping",
                aboutPanelOperator.isShowing());
        
        // test if similar card search still works
        addCardButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JListOperator similarCardsListOperator = new JListOperator(
                dialogOperator, new NameComponentChooser("similarCardsList"));
        frontSideTextAreaOperator.setText("Berlin");
        assertEquals(1, similarCardsListOperator.getModel().getSize());
    }
}
