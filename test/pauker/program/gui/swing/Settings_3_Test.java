/*
 * SplitPaneTest.java
 *
 * Created on 6. Dezember 2006, 18:50
 *
 */

package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * tests, if the splitpane position is remembered between Pauker starts
 * @author Ronny.Standtke@gmx.net
 */
public class Settings_3_Test extends TestCase {

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testSettings() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");
        
        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                frameOperator, new NameComponentChooser("splitPane"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JCheckBoxOperator showTimerCheckBoxOperator = new JCheckBoxOperator(
                frameOperator, new NameComponentChooser("showTimerCheckBox"));
        JButtonOperator cancelLearningButtonOperator =
                new JButtonOperator(frameOperator,
                new NameComponentChooser("cancelLearningButton"));

        // check that previous test moved the splitpane to the min position
        int minimumDividerLocation =
                splitPaneOperator.getMinimumDividerLocation();
        int dividerLocation = splitPaneOperator.getDividerLocation();

        assertEquals(dividerLocation, minimumDividerLocation);

        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator =
                new JTextAreaOperator(dialogOperator,
                new NameComponentChooser("reverseSideTextArea"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));
        assertEquals(new Font("Dialog", Font.PLAIN, 12),
                frontSideTextAreaOperator.getFont());
        assertEquals(Color.BLACK, frontSideTextAreaOperator.getForeground());
        assertEquals(Color.WHITE, frontSideTextAreaOperator.getBackground());
        assertEquals(new Font("Serif", Font.ITALIC, 8),
                reverseSideTextAreaOperator.getFont());
        assertEquals(Color.GREEN, reverseSideTextAreaOperator.getForeground());
        assertEquals(Color.GRAY, reverseSideTextAreaOperator.getBackground());
        frontSideTextAreaOperator.setText("front");
        reverseSideTextAreaOperator.setText("reverse");

        // reset values to default (so that I do not have to reset them manually
        // every time...)
        reverseSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 12));
        reverseSideTextAreaOperator.setForeground(Color.BLACK);
        reverseSideTextAreaOperator.setBackground(Color.WHITE);
        Tools.doClick(okButtonOperator);

        // check that timer settings from test 2 is preserverd
        Tools.doClick(learnNewCardsButtonOperator);
        assertTrue("timer settings are not preserved!",
                showTimerCheckBoxOperator.isSelected());
        Tools.doClick(cancelLearningButtonOperator);

        frameOperator.pressKey(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK);
        dialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("DontSave"));
        Tools.doClick(dontSaveButtonOperator);
        
        Thread.sleep(1000); // give some time to save the settings
    }
}
