/*
 * OpenLearnedUnlearnedTest.java
 *
 * Created on 5. August 2007, 09:55
 *
 */
package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JFrameOperator;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * here we test if lesson files are correctly opened that contain "learned" cards
 * in the unlearned batch
 * @author Ronny.Standtke@gmx.net
 */
public class OpenLearnedUnlearnedTest extends TestCase {

    public void testOpenLearnedUnlearned() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // get references to test lessons
        String internalPath = "/testLessons/UnlearnedLearned.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();

        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson lesson = paukerFrame.getCurrentLesson();
        assertEquals("lesson loading failed!",
                1, lesson.getNumberOfCards());
    }
}
