package pauker.program.gui.swing;

import java.lang.reflect.Field;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * There was the following bug:
 * - user started learning new cards but just waited for a complete timeout
 * - answered "no" when asked if the first card was remembered
 * - Pauker should have stopped here, instead it continued with wrong data
 * @author Ronny.Standtke@gmx.net
 */
public class TimeoutTest extends TestCase {

    public void testOpen() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // get path of test lesson
        String internalPath = "/testLessons/1.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JButtonOperator repeatingNoButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingNoButton"));

        // set short ustm/stm times
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Field ustmField = PaukerFrame.class.getDeclaredField("ustmTime");
        ustmField.setAccessible(true);
        ustmField.setInt(paukerFrame, 1); // USTM = 1 sec
        Field stmField = PaukerFrame.class.getDeclaredField("stmTime");
        stmField.setAccessible(true);
        stmField.setInt(paukerFrame, 2); // STM = 2 sec

        // start learning
        learnNewCardsButtonOperator.doClick();

        // wait for timeout
        Thread.sleep(3000);

        // say that we do not remember the card
        nextNewCardButtonOperator.doClick();
        showMeButtonOperator.doClick();
        repeatingNoButtonOperator.doClick();

        // test that Pauker stopped the learning process
        learnNewCardsButtonOperator.waitComponentShowing(true);
    }
}
