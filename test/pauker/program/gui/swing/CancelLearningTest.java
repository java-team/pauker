/*
 * CancelLearningTest.java
 *
 * Created on 12.10.2008, 13:22:37
 *
 */

package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.operators.JRadioButtonOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Batch;
import pauker.program.Card;
import pauker.program.CardSide;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * A test that checks that cancelling the learning process works as expected
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class CancelLearningTest extends TestCase {

    /**
     * runs the test
     * @throws Exception if an exception occurs
     */
    public void testCancelling() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator cancelLearningButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("cancelLearningButton"));
        JMenuOperator extraMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("extraMenu"));

        // configure Pauker to put cards back on top
        extraMenuOperator.doClick();
        JMenuItemOperator configureMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("configureMenuItem"));
        configureMenuItemOperator.push();
        JDialogOperator dialogOperator = new JDialogOperator();
        JRadioButtonOperator putBackTopRadioButtonOperator =
                new JRadioButtonOperator(dialogOperator,
                new NameComponentChooser("putBackTopRadioButton"));
        JButtonOperator okButtonOperator = new JButtonOperator(dialogOperator,
                new NameComponentChooser("okButton"));
        putBackTopRadioButtonOperator.setSelected(true);
        Tools.doClick(okButtonOperator);

        // add two cards
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                newDialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                newDialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        okButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("okButton"));
        // card 1
        newFrontSideTextAreaOperator.setText("1");
        reverseSideTextAreaOperator.setText("1");
        repeatingMethodComboBoxOperator.setSelectedIndex(1);
        keepOpenCheckBoxOperator.setSelected(true);
        Tools.doClick(okButtonOperator);
        // card 2
        newFrontSideTextAreaOperator.setText("2");
        reverseSideTextAreaOperator.setText("2");
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        // start learning
        Tools.doClick(learnNewCardsButtonOperator);
        Tools.doClick(nextNewCardButtonOperator);

        // cancel learning
        cancelLearningButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Cancel_Learning"));
        Tools.doClick(confirmButtonOperator);
        learnNewCardsButtonOperator.waitComponentShowing(true);

        // check order of cards
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson currentLesson = paukerFrame.getCurrentLesson();
        Batch unlearnedBatch = currentLesson.getUnlearnedBatch();
        Card firstCard = unlearnedBatch.getCard(0);
        CardSide frontSide = firstCard.getFrontSide();
        assertEquals("1", frontSide.getText());
    }
}
