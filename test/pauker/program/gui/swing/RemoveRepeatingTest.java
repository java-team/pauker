/*
 * RemoveRepeatingTest.java
 *
 * Created on 29. Juli 2007, 09:48
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * when removing a card while repeating there was a bug that the card was not removed
 * from the search engine
 * here we test if the card is also removed from the search engine
 * @author Ronny.Standtke@gmx.net
 */
public class RemoveRepeatingTest extends TestCase {

    public void testRemove() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get path of test lesson
        String internalPath = "/testLessons/RemoveRepeatingTest.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        JButtonOperator repeatCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatCardsButton"));
        JButtonOperator removeRepeatingButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("removeRepeatingButton"));
        JButtonOperator cancelRepeatingButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("cancelRepeatingButton"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JTextAreaOperator repeatingFrontSideTextArea = new JTextAreaOperator(
                frameOperator, new NameComponentChooser("repeatingFrontSideTextArea"));

        Tools.doClick(repeatCardsButtonOperator);

        // remove the first card
        repeatingFrontSideTextArea.waitComponentShowing(true);
        String removedCardText = repeatingFrontSideTextArea.getText();
        removeRepeatingButtonOperator.waitComponentEnabled();
        removeRepeatingButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Remove_Card"));
        Tools.doClick(confirmButtonOperator);
        Tools.doClick(cancelRepeatingButtonOperator);

        // test that card was also removed from summary
        assertEquals(1, paukerFrame.getCurrentLesson().getSummaryBatch().getNumberOfCards());

        // now add a new card
        addCardButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JListOperator similarCardsListOperator = new JListOperator(
                dialogOperator, new NameComponentChooser("similarCardsList"));
        frontSideTextAreaOperator.setText(removedCardText);
        int similarCardsCount = similarCardsListOperator.getModel().getSize();
        assertEquals("the removed card was not removed from the search engine!",
                0, similarCardsCount);
    }
}
