/*
 * RemoveFromUnlearnedTest.java
 *
 * Created on 29. Juli 2007, 14:04
 *
 */

package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComponentOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * here we test if removing a card from the "unlearned" batch also removes this
 * card from the summary batch and other stuff
 * @author Ronny.Standtke@gmx.net
 */
public class RemoveFromUnlearnedTest extends TestCase {

    /**
     * executes the test
     * @throws java.lang.Exception
     */
    public void testRemoveFromUnlearned() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get references to test lessons
        String internalPath = "/testLessons/SortingTest.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator notLearnedLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("notLearnedLabel"));
        JButtonOperator removeCardButton = new JButtonOperator(
                frameOperator, new NameComponentChooser("removeCardButton"));
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JButtonOperator repeatingYesButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingYesButton"));
        JButtonOperator repeatingNoButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingNoButton"));
        JButtonOperator removeUstmButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("removeUstmButton"));
        JComponentOperator transitionPanelOperator = new JComponentOperator(
                frameOperator, new NameComponentChooser("transitionPanel"));

        // load unlearned batch
        Tools.clickComponentOperator(notLearnedLabelOperator);
        batchListOperator.waitComponentShowing(true);
        assertEquals(7, batchListOperator.getModel().getSize());

        // remove first card
        batchListOperator.setSelectedIndex(0);
        removeCardButton.waitComponentShowing(true);
        removeCardButton.waitComponentEnabled();
        removeCardButton.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Remove_Card"));
        Tools.doClick(confirmButtonOperator);

        Tools.clickComponentOperator(summaryLabelOperator);
        Thread.sleep(500);
        assertEquals("removing card from unlearned batch did not remove the " +
                "card from the summary!",
                6, batchListOperator.getModel().getSize());

        /*
         * there was another bug where removing the last card from the unlearned
         * batch stopped the learning process while there were still cards in
         * the short-term memory
         */
        learnNewCardsButtonOperator.doClick();

        // put all cards into the ustm
        for (int i = 0; i < 6; i++) {
            nextNewCardButtonOperator.waitComponentShowing(true);
            nextNewCardButtonOperator.waitComponentEnabled();
            nextNewCardButtonOperator.doClick();
        }
        // skip transition
        repeatUSTMButtonOperator.waitComponentShowing(true);
        repeatUSTMButtonOperator.doClick();

        // put all cards but one into the stm
        for (int i = 0; i < 5; i++) {
            showMeButtonOperator.waitComponentShowing(true);
            showMeButtonOperator.doClick();
            repeatingYesButtonOperator.waitComponentShowing(true);
            repeatingYesButtonOperator.doClick();
        }
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.doClick();
        repeatingNoButtonOperator.waitComponentShowing(true);
        repeatingNoButtonOperator.doClick();

        // remove last card from unlearned batch
        removeUstmButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Remove_Card"));
        Tools.doClick(confirmButtonOperator);

        // now Pauker needs to continue with the STM instead of stopping
        transitionPanelOperator.waitComponentShowing(true);
    }
}
