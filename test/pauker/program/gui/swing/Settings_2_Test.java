/*
 * SplitPaneTest.java
 *
 * Created on 6. Dezember 2006, 18:50
 *
 */
package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * tests, if the splitpane position is remembered between Pauker starts
 * @author Ronny.Standtke@gmx.net
 */
public class Settings_2_Test extends TestCase {

    /**
     * runs the test
     * @throws java.lang.Exception if an exception occurs
     */
    public void testSettings() throws Exception {

        assertFalse("Only screenshots are generated!", 
                Screenshots.UPDATE_SCREENSHOTS);
        
        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");
        
        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                frameOperator, new NameComponentChooser("splitPane"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JCheckBoxOperator showTimerCheckBoxOperator = new JCheckBoxOperator(
                frameOperator, new NameComponentChooser("showTimerCheckBox"));
        JButtonOperator cancelLearningButtonOperator = 
                new JButtonOperator(frameOperator, 
                new NameComponentChooser("cancelLearningButton"));

        // check that previous test moved the splitpane to the max position
        int minimumDividerLocation = 
                splitPaneOperator.getMinimumDividerLocation();
        int maximumDividerLocation = 
                splitPaneOperator.getMaximumDividerLocation();
        int dividerLocation = splitPaneOperator.getDividerLocation();

        // check that settings from test 1 are preserved
        assertEquals(dividerLocation, maximumDividerLocation);

        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = 
                new JTextAreaOperator(dialogOperator, 
                new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = 
                new JComboBoxOperator(dialogOperator, 
                new NameComponentChooser("repeatingMethodComboBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));
        assertEquals(new Font("DialogInput", Font.BOLD, 20), 
                frontSideTextAreaOperator.getFont());
        assertEquals(Color.BLUE, frontSideTextAreaOperator.getForeground());
        assertEquals(Color.YELLOW, frontSideTextAreaOperator.getBackground());
        assertEquals(new Font("Dialog", Font.PLAIN, 12), 
                reverseSideTextAreaOperator.getFont());
        assertEquals(Color.BLACK, reverseSideTextAreaOperator.getForeground());
        assertEquals(Color.WHITE, reverseSideTextAreaOperator.getBackground());
        assertEquals(0, repeatingMethodComboBoxOperator.getSelectedIndex());

        // now change the font again
        frontSideTextAreaOperator.setText("front");
        frontSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 12));
        frontSideTextAreaOperator.setForeground(Color.BLACK);
        frontSideTextAreaOperator.setBackground(Color.WHITE);
        reverseSideTextAreaOperator.setText("reverse");
        reverseSideTextAreaOperator.setFont(new Font("Serif", Font.ITALIC, 8));
        reverseSideTextAreaOperator.setForeground(Color.GREEN);
        reverseSideTextAreaOperator.setBackground(Color.GRAY);

        Tools.doClick(okButtonOperator);

        // check that timer settings from test 1 is preserverd
        Tools.doClick(learnNewCardsButtonOperator);
        assertFalse("timer settings are not preserved!",
                showTimerCheckBoxOperator.isSelected());
        showTimerCheckBoxOperator.setSelected(true);
        Tools.doClick(cancelLearningButtonOperator);
        
        // set divider to minimum location
        splitPaneOperator.setDividerLocation(minimumDividerLocation);
        
        // exit
        Thread.sleep(1000);
        frameOperator.pressKey(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK);
        dialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("DontSave"));
        Tools.doClick(dontSaveButtonOperator);
        
        Thread.sleep(1000);
    }
}
