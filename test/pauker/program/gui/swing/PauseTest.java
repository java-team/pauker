/*
 * PauseTest.java
 *
 * Created on 20.10.2008, 11:19:27
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.operators.JToggleButtonOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * Tests, if pressing the pause button works as expected
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class PauseTest extends TestCase {

    /**
     * runs the test
     * @throws java.lang.Exception if an exception occurs
     */
    public void testPause() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        final JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JButtonOperator typingOKButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("typingOKButton"));
        JToggleButtonOperator pauseLearningToggleButtonOperator =
                new JToggleButtonOperator(frameOperator,
                new NameComponentChooser("pauseLearningToggleButton"));
        JButtonOperator moveBackButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("moveBackButton"));
        JButtonOperator ignoreErrorButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("ignoreErrorButton"));

        // insert a card
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                newDialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                newDialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("okButton"));
        newFrontSideTextAreaOperator.setText("1");
        reverseSideTextAreaOperator.setText("2");
        repeatingMethodComboBoxOperator.setSelectedIndex(0);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        // "learn" the one and only card and mistype answer
        Tools.doClick(learnNewCardsButtonOperator);
        Tools.doClick(nextNewCardButtonOperator);
        Tools.doClick(repeatUSTMButtonOperator);
        Tools.doClick(typingOKButtonOperator);
        
        // test pause
        Tools.doClick(pauseLearningToggleButtonOperator);
        assertFalse(moveBackButtonOperator.isEnabled());
        assertFalse(ignoreErrorButtonOperator.isEnabled());
        
        // test unpause
        Tools.doClick(pauseLearningToggleButtonOperator);
        assertTrue(moveBackButtonOperator.isEnabled());
        assertTrue(ignoreErrorButtonOperator.isEnabled());

    }
}
