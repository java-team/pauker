/*
 * SplitPaneTest.java
 *
 * Created on 6. Dezember 2006, 18:50
 *
 */

package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * tests, if the splitpane position is remembered between Pauker starts
 * @author Ronny.Standtke@gmx.net
 */
public class Settings_1_Test extends TestCase {

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testSettings() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();

        // enlarge frame here a bit
        // (needed for next splitpane test in Settings_1_Test)
        Dimension size = frameOperator.getSize();
        frameOperator.setSize(
                (int) size.getWidth(), (int) size.getHeight() + 200);

        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                frameOperator, new NameComponentChooser("splitPane"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JCheckBoxOperator showTimerCheckBoxOperator = new JCheckBoxOperator(
                frameOperator, new NameComponentChooser("showTimerCheckBox"));
        JButtonOperator cancelLearningButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("cancelLearningButton"));

        // move splitpane
        int minimumDividerLocation =
                splitPaneOperator.getMinimumDividerLocation();
        int maximumDividerLocation =
                splitPaneOperator.getMaximumDividerLocation();

        if (minimumDividerLocation == maximumDividerLocation) {
            fail("can not execute test when " +
                    "minimumDividerLocation == maximumDividerLocation");
        }

        // set divider to maximum location
        splitPaneOperator.setDividerLocation(maximumDividerLocation);

        // set some font properties
        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator =
                new JTextAreaOperator(dialogOperator,
                new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator =
                new JComboBoxOperator(dialogOperator,
                new NameComponentChooser("repeatingMethodComboBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));
        frontSideTextAreaOperator.setFont(
                new Font("DialogInput", Font.BOLD, 20));
        frontSideTextAreaOperator.setText("front");
        frontSideTextAreaOperator.setForeground(Color.BLUE);
        frontSideTextAreaOperator.setBackground(Color.YELLOW);
        reverseSideTextAreaOperator.setText("reverse");
        reverseSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 12));
        reverseSideTextAreaOperator.setForeground(Color.BLACK);
        reverseSideTextAreaOperator.setBackground(Color.WHITE);
        repeatingMethodComboBoxOperator.setSelectedIndex(0);
        Tools.doClick(okButtonOperator);

        // disable timer (check in next test...)
        Tools.doClick(learnNewCardsButtonOperator);
        showTimerCheckBoxOperator.setSelected(false);
        Tools.doClick(cancelLearningButtonOperator);

        Thread.sleep(1000);
        frameOperator.pressKey(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK);
        dialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("DontSave"));
        Tools.doClick(dontSaveButtonOperator);

        Thread.sleep(1000);
    }
}
