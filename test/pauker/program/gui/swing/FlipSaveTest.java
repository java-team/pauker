/*
 * FlipSaveTest.java
 *
 * Created on 11. Oktober 2007, 15:25
 *
 */
package pauker.program.gui.swing;

import java.io.File;
import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFileChooserOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * Tests that flipped card info is preserved when saving/opening a lesson
 *
 * @author Ronny.Standtke@gmx.net
 */
public class FlipSaveTest extends TestCase {

    public void testFlipSave() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JButtonOperator repeatingYesButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingYesButton"));
        JButtonOperator repeatSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatSTMButton"));
        JButtonOperator transitionOKButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("transitionOKButton"));
        JMenuOperator fileMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("fileMenu"));
        JButtonOperator saveButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("saveButton"));
        JButtonOperator openButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("openButton"));

        // add a new card
        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                dialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                dialogOperator, new NameComponentChooser("keepOpenCheckBox"));

        frontSideTextAreaOperator.setText("Front");
        reverseSideTextAreaOperator.setText("Reverse");
        repeatingMethodComboBoxOperator.setSelectedIndex(1);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        // "learn" the one and only card
        learnNewCardsButtonOperator.waitComponentEnabled();
        learnNewCardsButtonOperator.doClick();
        nextNewCardButtonOperator.waitComponentShowing(true);
        nextNewCardButtonOperator.waitComponentEnabled();
        nextNewCardButtonOperator.doClick();
        repeatUSTMButtonOperator.waitComponentShowing(true);
        repeatUSTMButtonOperator.doClick();
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.doClick();
        repeatingYesButtonOperator.waitComponentShowing(true);
        repeatingYesButtonOperator.doClick();
        repeatSTMButtonOperator.waitComponentShowing(true);
        repeatSTMButtonOperator.doClick();
        transitionOKButtonOperator.waitComponentShowing(true);
        transitionOKButtonOperator.doClick();
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.doClick();
        repeatingYesButtonOperator.waitComponentShowing(true);
        repeatingYesButtonOperator.doClick();

        // flip the lesson
        fileMenuOperator.doClick();
        JMenuItemOperator flipCardSidesMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("flipCardSidesMenuItem"));
        flipCardSidesMenuItemOperator.push();
        dialogOperator = new JDialogOperator();
        JButtonOperator flipButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Flip_Card_Sides"));
        Tools.doClick(flipButtonOperator);

        // save the lesson
        assertTrue(saveButtonOperator.isEnabled());
        saveButtonOperator.pushNoBlock();
        JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
        File tmpFile = File.createTempFile("FlipSaveTest", ".pau.gz");
        assertTrue("Could not delete temporary file!", tmpFile.delete());
        fileChooserOperator.setSelectedFile(tmpFile);
        fileChooserOperator.approveSelection();

        // re-open lesson
        assertTrue(openButtonOperator.isEnabled());
        openButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        fileChooserOperator = new JFileChooserOperator();
        fileChooserOperator.setSelectedFile(tmpFile);
        JButtonOperator dialogOpenButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("openButton"));
        Tools.doClick(dialogOpenButtonOperator);

        // flip the lesson again
        fileMenuOperator.doClick();
        flipCardSidesMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("flipCardSidesMenuItem"));
        flipCardSidesMenuItemOperator.push();
        dialogOperator = new JDialogOperator();
        flipButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Flip_Card_Sides"));
        Tools.doClick(flipButtonOperator);
        Thread.sleep(1000);

        // check that card is in batch 1
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson lesson = paukerFrame.getCurrentLesson();
        assertEquals(1, lesson.getLongTermBatch(0).getNumberOfCards());
    }
}
