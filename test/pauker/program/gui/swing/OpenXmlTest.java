/*
 * OpenTest.java
 *
 * Created on 24. Juni 2006, 11:14
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package pauker.program.gui.swing;

import java.io.File;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.JMenuItem;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFileChooserOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class OpenXmlTest extends TestCase {

    public void testOpen() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get references to test lessons
        String internalPath = "/testLessons/1.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        File testFile1 = new File(testFilePath);
        internalPath = "/testLessons/2.pau.gz";
        testFilePath = getClass().getResource(internalPath).getPath();
        File testFile2 = new File(testFilePath);

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator newButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("newButton"));
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JMenuOperator fileMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("fileMenu"));

        // check, if command line open was successfull
        String summaryString = strings.getString("SummaryLabel");
        String formattedSummary = MessageFormat.format(summaryString,
                Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(1));
        assertEquals(formattedSummary, summaryLabelOperator.getText());


        Tools.doClick(newButtonOperator);


        // test opening via menu item
        fileMenuOperator.doClick();
        JMenuItemOperator openMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("openMenuItem"));
        openMenuItemOperator.push();
        JDialogOperator openFileDialogOperator = new JDialogOperator();
        JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
        fileChooserOperator.setSelectedFile(testFile1);
        JButtonOperator openButtonOperator = new JButtonOperator(
                openFileDialogOperator, new NameComponentChooser("openButton"));
        Tools.doClick(openButtonOperator);
        assertEquals(formattedSummary, summaryLabelOperator.getText());

        // test merging of lessons
        fileMenuOperator.doClick();
        openMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("openMenuItem"));
        openMenuItemOperator.push();
        openFileDialogOperator = new JDialogOperator();
        fileChooserOperator = new JFileChooserOperator();
        JCheckBoxOperator mergingCheckBoxOperator = new JCheckBoxOperator(
                openFileDialogOperator, new NameComponentChooser("mergingCheckBox"));
        mergingCheckBoxOperator.setSelected(true);
        File[] selectedFiles = new File[]{testFile1, testFile2};
        fileChooserOperator.setSelectedFiles(selectedFiles);
        openButtonOperator = new JButtonOperator(
                openFileDialogOperator, new NameComponentChooser("openButton"));
        Tools.doClick(openButtonOperator);
        formattedSummary = MessageFormat.format(summaryString,
                Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(3));
        assertEquals(formattedSummary, summaryLabelOperator.getText());


        // test recent file menu
        fileMenuOperator.doClick();
        JMenuOperator recentFilesMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("recentFilesMenu"));
        recentFilesMenuOperator.doClick();
        final JMenuItemOperator recentFilesMenuItem1Operator = new JMenuItemOperator(
                (JMenuItem) recentFilesMenuOperator.getMenuComponent(0));

        recentFilesMenuItem1Operator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("DontSave"));
        Tools.doClick(dontSaveButtonOperator);

        formattedSummary = MessageFormat.format(summaryString,
                Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(1));
        assertEquals(formattedSummary, summaryLabelOperator.getText());
    }
}
