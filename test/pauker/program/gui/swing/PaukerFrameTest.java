/*
 * PaukerFrameTest.java
 * JUnit based test
 *
 * Created on 26. August 2007, 15:43
 */
package pauker.program.gui.swing;

import java.lang.reflect.Method;
import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 * Tests for methods within PaukerFrame
 * @author Ronny.Standtke@gmx.net
 */
public class PaukerFrameTest extends TestCase {

    /**
     * This test is related to a problem where Pauker was throwing an exception if we
     * put back cards to an empty unlearned batch and used the random putback strategy.
     * @throws java.lang.Exception if the method is not found
     */
    public void testNextRandom() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        Method nextRandomMethod = PaukerFrame.class.getDeclaredMethod("nextRandom", int.class);
        nextRandomMethod.setAccessible(true);
        PaukerFrame paukerFrame = new PaukerFrame();
        assertEquals(0, nextRandomMethod.invoke(paukerFrame, 0));
    }
}
