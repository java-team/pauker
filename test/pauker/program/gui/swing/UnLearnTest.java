/*
 * UnLearnTest.java
 *
 * Created on 9. September 2007, 13:22
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import javax.swing.ListModel;
import javax.swing.border.TitledBorder;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComponentOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * test that unlearning is not too slow...
 * @author Ronny.Standtke@gmx.net
 */
public class UnLearnTest extends TestCase {

    /**
     * runs the test
     * @throws java.lang.Exception if an exception occurs
     */
    public void testUnlearn() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        // set up exception handler
        MyUEH myUEH = new MyUEH();
        Thread.setDefaultUncaughtExceptionHandler(myUEH);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get path of test lesson
        String internalPath = "/testLessons/Riesentest.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        JLabelOperator notLearnedLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("notLearnedLabel"));
        JComponentOperator batchPanelOperator = new JComponentOperator(
                frameOperator, new NameComponentChooser("batchPanel"));
        TitledBorder batchBanelBorder = (TitledBorder) batchPanelOperator.getBorder();
        JLabelOperator firstLongTermBatchLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("longTermBatchLabel0"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator instantRepeatButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("instantRepeatButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator forgetButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("forgetButton"));
        JButtonOperator fontButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("fontButton"));
        JButtonOperator learningMethodButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learningMethodButton"));
        JButtonOperator repeatCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatCardsButton"));

        assertEquals("there was an exception on startup!",
                0, myUEH.getCounter());

        // select unlearned batch
        Tools.clickComponentOperator(notLearnedLabelOperator);
        batchPanelOperator.waitComponentShowing(true);
        assertEquals(strings.getString("Not_learned"), batchBanelBorder.getTitle());

        // select first long term batch
        Tools.clickComponentOperator(firstLongTermBatchLabelOperator);
        Thread.sleep(1000);
        String title = strings.getString("Batch") + ' ' + 1;
        assertEquals(title, batchBanelBorder.getTitle());

        // programmatically load unlearned batch again
        Lesson currentLesson = paukerFrame.getCurrentLesson();
        paukerFrame.loadBatch(currentLesson.getUnlearnedBatch());

        // instant repeat all unlearned cards
        ListModel listModel = batchListOperator.getModel();
        int size = listModel.getSize();
        batchListOperator.setSelectionInterval(0, size - 1);
        instantRepeatButtonOperator.waitComponentEnabled();
        instantRepeatButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Repeat_Cards_Instantly"));
        long start = System.currentTimeMillis();
        Tools.doClick(confirmButtonOperator);
        assertFalse(fontButtonOperator.isEnabled());
        assertFalse(learningMethodButtonOperator.isEnabled());
        repeatCardsButtonOperator.waitComponentEnabled();
        long stop = System.currentTimeMillis();
        long instantRepeatTime = stop - start;

        // now move them all back to the unlearned batch
        paukerFrame.loadBatch(currentLesson.getLongTermBatch(0));
        listModel = batchListOperator.getModel();
        size = listModel.getSize();
        batchListOperator.setSelectionInterval(0, size - 1);
        forgetButtonOperator.waitComponentEnabled();
        forgetButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Move_Cards_Back"));
        start = System.currentTimeMillis();
        Tools.doClick(confirmButtonOperator);
        assertFalse(repeatCardsButtonOperator.isEnabled());
        assertFalse(fontButtonOperator.isEnabled());
        assertFalse(learningMethodButtonOperator.isEnabled());
        assertTrue(learnNewCardsButtonOperator.isEnabled());
        stop = System.currentTimeMillis();
        long unlearnTime = stop - start;

        // check that instant repeat and unlearn do not differ substantially in time
        long max = Math.max(instantRepeatTime, unlearnTime);
        long min = Math.min(instantRepeatTime, unlearnTime);

        assertTrue("times for instant repeat (" + instantRepeatTime +
                "ms) and unlearn (" + unlearnTime + 
                " ms) differ substantially!", (max / min) < 4);
    }

    private static class MyUEH implements Thread.UncaughtExceptionHandler {

        private int counter;

        public void uncaughtException(Thread thread, Throwable throwable) {
            throwable.printStackTrace();
            counter++;
        }

        public int getCounter() {
            return counter;
        }
    }
}
