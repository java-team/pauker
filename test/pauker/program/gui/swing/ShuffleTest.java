/*
 * ShuffleTest.java
 *
 * Created on 14.08.2008, 21:34:10
 *
 */

package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Batch;
import pauker.program.Card;
import pauker.program.CardSide;
import pauker.program.Lesson;
import pauker.program.LongTermBatch;
import screenshots.Screenshots;

/**
 * Here we test that shuffling the summary batch will also shuffle cards
 * in the unlearned batch and in the long term batches
 * @author ronny
 */
public class ShuffleTest extends TestCase {

    /**
     * runs the test
     * @throws Exception if any exception occurs
     */
    public void testSorting() throws Exception {
        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator randomizeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("randomizeButton"));

        // fill in example lesson
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson lesson = paukerFrame.getCurrentLesson();
        LongTermBatch longTermBatch = lesson.addLongTermBatch();
        for (int i = 0; i < 10; i++) {
            // add longterm card
            CardSide frontSide = new CardSide("long term front " + i);
            CardSide reverseSide = new CardSide("long term reverse " + i);
            Card longTermCard = new Card(frontSide, reverseSide);
            longTermBatch.addCard(longTermCard);

            // add unlearned card
            frontSide = new CardSide("unlearned front " + i);
            reverseSide = new CardSide("unlearned reverse " + i);
            Card unlearnedCard = new Card(frontSide, reverseSide);
            paukerFrame.addCard(unlearnedCard);
        }

        // load summary
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);

        // select first card
        batchListOperator.setSelectedIndex(0);

        // randomize summary
        try {
            Tools.doClick(randomizeButtonOperator);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        // now check that both the unlearned and the long term batch are
        // shuffled
        Batch unlearnedBatch = lesson.getUnlearnedBatch();
        boolean shuffled = false;
        for (int i = 0, numberOfCards = unlearnedBatch.getNumberOfCards(); i < numberOfCards; i++) {
            Card card = unlearnedBatch.getCard(i);
            if (!card.getFrontSide().getText().equals("unlearned front " + i)) {
                // good, there is a card outside the previous order
                shuffled = true;
                break;
            }
        }
        assertTrue("unlearned batch was not shuffled", shuffled);

        shuffled = false;
        for (int i = 0, numberOfCards = longTermBatch.getNumberOfCards(); i < numberOfCards; i++) {
            Card card = longTermBatch.getCard(i);
            if (!card.getFrontSide().getText().equals("long term front " + i)) {
                // good, there is a card outside the previous order
                shuffled = true;
                break;
            }
        }
        assertTrue("long term batch was not shuffled", shuffled);

        // check selection
        int[] selectedIndices = batchListOperator.getSelectedIndices();
        assertEquals("the number of selections changed after shuffling the summary!",
                1, selectedIndices.length);
        final int NEW_CARD_INDEX = selectedIndices[0];
        assertTrue("scrolling to selected cards after shuffling the summary did not work!",
                (batchListOperator.getFirstVisibleIndex() <= NEW_CARD_INDEX) &&
                (batchListOperator.getLastVisibleIndex() >= NEW_CARD_INDEX));
    }
}
