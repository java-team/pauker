/*
 * RepeatFontTest.java
 *
 * Created on 4. Dezember 2006, 21:13
 *
 */
package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Font;
import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class RepeatFontTest extends TestCase {

    // Bug#1602290 test that style gets applied when repeating by typing
    public void testRepeatFonts() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        final JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JTextAreaOperator repeatingInsertTextAreaOperator = new JTextAreaOperator(
                frameOperator, new NameComponentChooser("repeatingInsertTextArea"));
        JButtonOperator typingOKButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("typingOKButton"));
        JButtonOperator removeUstmButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("removeUstmButton"));
        JButtonOperator cancelLearningButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("cancelLearningButton"));

        // fill example card
        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                dialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                dialogOperator, new NameComponentChooser("keepOpenCheckBox"));

        frontSideTextAreaOperator.setText("1");
        frontSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 20));
        reverseSideTextAreaOperator.setText("2");
        Font reverseSideFont = new Font("Dialog", Font.PLAIN, 20);
        reverseSideTextAreaOperator.setFont(reverseSideFont);
        reverseSideTextAreaOperator.setForeground(Color.RED);
        reverseSideTextAreaOperator.setBackground(Color.BLUE);
        repeatingMethodComboBoxOperator.setSelectedIndex(0);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        Tools.doClick(learnNewCardsButtonOperator);
        Tools.doClick(nextNewCardButtonOperator);
        Tools.doClick(repeatUSTMButtonOperator);
        Font currentFont = repeatingInsertTextAreaOperator.getFont();
        assertEquals(reverseSideFont, currentFont);
        repeatingInsertTextAreaOperator.setText("Test");
        assertEquals(Color.WHITE, repeatingInsertTextAreaOperator.getBackground());
        Color foreground = repeatingInsertTextAreaOperator.getForeground();
        assertEquals(Color.BLACK, foreground);

        // test that the "OK" button is the default button when we repeat cards
        // by typing
        assertTrue("OK button was not the default button!",
                typingOKButtonOperator.isDefaultButton());

        // cancel learning
        cancelLearningButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Cancel_Learning"));
        Tools.doClick(confirmButtonOperator);
        learnNewCardsButtonOperator.waitComponentShowing(true);

        // test that removing the card while learning removes it from the summary batch
        learnNewCardsButtonOperator.doClick();
        assertTrue(removeUstmButtonOperator.isShowing());
        assertTrue(removeUstmButtonOperator.isEnabled());
        removeUstmButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Remove_Card"));
        Tools.doClick(confirmButtonOperator);
        addCardButtonOperator.waitComponentShowing(true);
        assertEquals("card was not removed from summary batch!",
                0, paukerFrame.getCurrentLesson().getSummaryBatch().getNumberOfCards());
    }
}
