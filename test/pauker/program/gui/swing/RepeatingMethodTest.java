/*
 * RepeatingMethodTest.java
 *
 * Created on 19. Oktober 2007, 19:56
 *
 */
package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class RepeatingMethodTest extends TestCase {

    /**
     * here we test if the repeating method is also saved when we cancel the
     * new card dialog
     */
    public void testRepeatingMethod() throws Exception {

        assertFalse("Only screenshots are generated!", Screenshots.UPDATE_SCREENSHOTS);

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        final JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));

        // open new card dialog
        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                dialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JButtonOperator cancelButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("cancelButton"));

        // change repeating method
        int selectedIndex = repeatingMethodComboBoxOperator.getSelectedIndex();
        repeatingMethodComboBoxOperator.setSelectedIndex(selectedIndex == 0 ? 1 : 0);

        // cancel dialog
        cancelButtonOperator.doClick();

        // reopen new card dialog
        addCardButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        repeatingMethodComboBoxOperator = new JComboBoxOperator(
                dialogOperator, new NameComponentChooser("repeatingMethodComboBox"));

        // check that repeating method has changed
        assertFalse("repeating method not saved when closing the dialog!",
                repeatingMethodComboBoxOperator.getSelectedIndex() == selectedIndex);
    }
}
