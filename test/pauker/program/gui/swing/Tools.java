/*
 * Tools.java
 *
 * Created on 8. Oktober 2006, 11:12
 *
 */

package pauker.program.gui.swing;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.jemmy.operators.AbstractButtonOperator;
import org.netbeans.jemmy.operators.ComponentOperator;

/**
 * Some helper functions for JUnit/Jemmy unit tests
 * @author Ronny.Standtke@gmx.net
 */
public final class Tools {

    private static final Logger logger = 
            Logger.getLogger(Tools.class.getName());
    private static Robot robot;

    static {
        try {
            robot = new Robot();
        } catch (AWTException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    private Tools() {
        // singleton
    }

    /**
     * clicks the middle of a componentOperator
     * @param componentOperator the ComponentOperator to click
     */
    public static void clickComponentOperator(
            ComponentOperator componentOperator) {
        Point location = componentOperator.getLocationOnScreen();
        Dimension dimension = componentOperator.getSize();
        robot.mouseMove(
                location.x + dimension.width / 2,
                location.y + dimension.height / 2);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    /**
     * tests, that a button is visible and enabled and then clicks the button
     * @param buttonOperator the operator for the button
     * @throws java.lang.InterruptedException if an interruption occured
     */
    public static void doClick(AbstractButtonOperator buttonOperator) throws
            InterruptedException {
        buttonOperator.waitComponentShowing(true);
        buttonOperator.waitComponentEnabled();
        buttonOperator.doClick();
    }
}
