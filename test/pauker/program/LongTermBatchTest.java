/*
 * LongTermBatchTest.java
 * JUnit based test
 *
 * Created on 15. September 2007, 07:13
 */

package pauker.program;

import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class LongTermBatchTest extends TestCase {
    
    /**
     * Test of getExpiredCard method, of class pauker.program.LongTermBatch.
     */
    public void testGetExpiredCard() {
        
        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);
        
        // here we test if we get the "newest" card first
        
        LongTermBatch longTermBatch = new LongTermBatch(0);
        
        // add the "first" card
        CardSide firstCardSide = new CardSide();
        Card firstCard = new Card(firstCardSide, firstCardSide);
        longTermBatch.addCard(firstCard);
        firstCard.expire();
        // set expiration time further back
        firstCardSide.setLearnedTimestamp(System.currentTimeMillis() -
                longTermBatch.getExpirationTime() - 100000);
        
        // add some morde cards with random learned date
        for (int i = 0; i < 100; i++) {
            CardSide tmpCardSide = new CardSide();
            Card card = new Card(tmpCardSide, tmpCardSide);
            longTermBatch.addCard(card);
            card.expire();
        }
        longTermBatch.refreshExpiration();
        
        Card expiredCard = longTermBatch.getOldestExpiredCard();
        assertEquals("did not get the card with the oldest \"learned\" time stamp!",
                firstCard, expiredCard);
    }
}
