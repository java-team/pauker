<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE helpset
  PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 1.0//EN"
         "http://java.sun.com/products/javahelp/helpset_1_0.dtd">
<helpset xml:lang=fr version="1.0">
   <!-- title -->
   <title>Documentation - Pauker</title>

   <!-- maps -->
   <maps>
     <homeID>Index</homeID>
     <mapref location="fr/jhelpmap.jhm" />
   </maps>

   <!-- views -->
   <view>
      <name>TOC</name>
      <label>Table des matières</label>
      <type>javax.help.TOCView</type>
      <data>fr/jhelptoc.xml</data>
   </view>
   <view>
      <name>Index</name>
      <label>Index</label>
      <type>javax.help.IndexView</type>
      <data>fr/jhelpidx.xml</data>
   </view>
   <view>
      <name>Search</name>
      <label>Recherche</label>
      <type>javax.help.SearchView</type>
      <data engine="com.sun.java.help.search.DefaultSearchEngine">
         JavaHelpSearch_fr
      </data>
   </view>

</helpset>
