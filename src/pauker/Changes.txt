20081225
    - RELEASE: 1.8

20081214
    - RELEASE: 1.8RC7
    - FIX: Lesson files are no longer corrupted when an OutOfMemoryError occurs when saving a lesson.

20081206
    - NEW: Cancel button for text import dialog
    - FIX: encapsulate integer parsing when reading a lesson file with try-catch

20081117
    - RELEASE: 1.8RC6
    - NEW: use logging & i18n for startup debugging output
    - NEW: show warning message when heap is small

20081115
    - FIX: catch and report OutOfMemoryError when saving lesson
    - FIX: enlarged maximum heap size to 128 MByte

20081020
    - RELEASE: 1.8RC5
    - FIX: do not use XML-1.1 (does more harm than good)
    - FIX: disable typing error buttons when pausing
    - FIX: save "Show timer" settings

20081012
    - RELEASE: 1.8RC4
    - INTERNAL: update to Lucene 2.4.0
    - FIX: respect putback strategy when cancelling learning

20080929
    - FIX: filter XML text for Java versions < 1.6

20080920
    - FIX: use XMLv1.1 (only works with Java6) to support special characters
    - NEW: remember open file dialog size

20080814
    - FIX: shuffle unlearned batch and all longterm batches when shuffling the
           summary batch

20080813
    - NEW: added Vietnamese locale
    - FIX: recognize BOM at the beginning of CSV files
    - FIX: do not load summary batch after flipping a lesson

20080510
    - INTERNAL: update to Lucene 2.3.2

20080323
    - FIX: do NOT use the platform look & feel (still problems with GTK...)
    - FIX: sort order was inverted

20080317
    - RELEASE: 1.8RC3

20080308
    - FIX: scroll text to top when repeating

20080225
    - NEW: updated internal search engine to Apache Lucene 2.3.1
    - NEW: updated internal browser launcher to BrowserLauncher2 v1.3
    - NEW: use the platform look & feel

20080217
    - FIX: do not stop learning when removing the last card from the unlearned
           batch and remaining cards in the STM

20080202
    - FIX: do not shrink window when opening lesson
    - FIX: save and restore font dialog side selection
    - FIX: fixed handling of "null" background in font dialog

20080106
    - FIX: handling of empty STM batch was broken

20071214
    - FIX: updated JavaHelp from 2.0_02 to 2.0_05 (fixes search highlighting)

20071127
    - FIX: could not reset lessons with only three or less long term batches

20071104
    - NEW: Polish translation update

20071020
    - RELEASE: 1.8RC2
    - FIX: repeating method was not restored when opening new card dialog

20071011
    - FIX: flipping card sides after saving/opening a lesson had an offset error

20070915
    - FIX: always return oldest expired card from long term batch

20070914
    - FIX: removing from summary batch when removing while learning/repeating

20070912
    - INTERNAL: switched all credits files to UTF-8
    - FIX: restore summary list selection when using instant repeat or moving
           cards back to the unlearned batch
    - NEW: speedup for some card moving operations

20070910
    - FIX: use HashSet instead of ArrayList for expired cards
           (significant speedup when removing many cards from a long term batch)

20070909
    - FIX: open lessons given on command line within the Swing EHT
    - FIX: batch panel border must be repainted after changes
    - FIX: button for repeatin expired cards was not correctly disabled when
           "forgetting" cards

20070907
    - INTERNAL: ant target for deploying pauker_testing.jar via scp

20070906
    - FIX: some corrections to the French translation
    - INTERNAL: automatically set releaseinfo in docbook files
    - NEW: Slovak translation

20070902
    - NEW: updated Esperanto locale
    - NEW: updated Dutch locale

20070901
    - NEW: added a Swiss German locale
    - RELEASE: 1.8RC
    - INTERNAL: build system recognizes Pauker version number

20070827
    - NEW: updated Polish translation

20070826
    - FIX: dropping a card when unlearned batch is empty and putback strategy
           is random generated an exception

20070825
    - NEW: updated Spanish translation
    - FIX: calculation of next expiration date was broken

20070818
    - FIX: rounding error in expiration formula

20070815
    _ RELEASE: 1.8beta

20070805
    - FIX: made lesson XML file parsing more robust
           (can now handle learned cards in unlearned batch)

20070801
    - NEW: support for Java Web Start command line option "-open"
    - INTERNAL: use enums instead of "lots of static final int"s
    - FIX: use plural on button text when removing several cards

20070729
    - FIX: restoring selection after sorting a batch
    - FIX: scroll to selected cards after sorting a batch

20070728
    - INTERNAL: more fine grained lesson structure (different kind of batches)
    - FIX: OK button was not the default button when repeating a card by typing
    - NEW: improved FileChooserAccessory in file Open/Save dialogs

20070707
    - NEW: switched to GPLv3 (as now shown in About Dialog)

20070701
    - INTERNAL: update to Lucene-2.2.0

20070611
    - NEW: show/hide toolbar, remember location and visibility between starts

20070610
    - NEW: "real" toolbar
    - FIX: fixed restoring splitpane value at startup (do not set values > max)
    - NEW: Spanish translation
    - NEW: cards can be removed while learning/repeating
    - FIX: tooltips on small buttons when learning/repeating
    - FIX: searching for similar cards was broken after flipping card sides
    - INTERNAL: use enhanced for-loop where possible
                replaced StringBuffer with StringBuilder
                use varargs for MessageFormat.format(...)

20070609
    - NEW: much more sophisticated popup menu for text edit operations

20070603
    - COSMETICAL: removed donation button, added donation help menu entry

20070526
    - INTERNAL: global handling of NewCardDialog preferences

20070520
    - FIX: default button handling when running on Windows was problematic
    - INTERNAL: updated to Java-1.5

20070519
    - NEW: popup menu for simple text edit operations
    - FIX: forgetting cards did only work from summary view

20070516
    - RELEASE: 1.7.5

20070427
    - INTERNAL: update to Lucene-2.1.0
    - NEW: donation dialog

20070426
    - NEW: remember repeating method between starts

20070425
    - NEW: special handling of newlines in diff view
    - INTERNAL: use KeyboardFocusManager for setting default button

20070421
    - FIX: updated French documentation
    - FIX: use correct file filter when opening unpacked recent file
    - FIX: use correct method when opening unpacked lesson with the "accept all"
           file filter

20070303
    - RELEASE: 1.7.4

20070216
    - NEW: multiple selection in file open dialog when merging lessons

20070211
    - NEW: French translation of the online documentation

20070202
    - INTERNAL: Update to BrowserLauncher2-1.0

20070129
    - FIX: reactivate save button when editing in similar cards list
    - FIX: activate save button when merging lessons

20061218
    - RELEASE: 1.7.3

20061204
    - NEW: save font and colors of NewCardDialog in properties

20061203
    - FIX: apply text style to input area when repeating by typing

20061203
    - FIX: saving lesson when file parameter was without subdirectory throwed a
           NPE

20061202
    - INTERNAL: all documentation migrated to docbook

20061029
    - INTERNAL: automatic screenshot generation with JUnit/Jemmy (still
                incomplete)
    - NEW: updated French translation

20061028
    - NEW: updated "About" dialog with BrowserLauncher

20061008
    - NEW: determine "average" font & colors of selected cards when opening the
           font dialog
    - FIX: wrong replay icon was displayed because of incomplete renderer
           initialization

20060923
    - RELEASE: 1.7.2
    - FIX: tolerate missing quotes in CSV parser

20060801
    - FIX: NPE when saving CSV files

20060712
    - FIX: fixed opening help when using Java Web Start

20060624
    - RELEASE: 1.7.1
    - FIX: "&" in help toc
    - NEW: updated included credits list
    - NEW: Java Web Start

20060618
    - FIX: opening files from command line and recent file menu did not work

20060528
    - RELEASE: 1.7
    - NEW: update to Lucene-2.0.0

20060326
    - FIX: init card side text with "" instead of null (prevents later NPE's)
    - FIX: CSV import works now
    - NEW: update to Lucene-1.9.1
    - NEW: removed TSV & SSV (they are redundant, we just need CSV)

20060319
    - FIX: escape quotes when exporting to CSV

20060227
    - FIX: incorrect arrow buttons update when adding cards
    - FIX: switch away from large summary or unlearned batch when adding cards
           (speedup)
    - NEW: CSV export

20060224
    - FIX: line wrapping in batch list now REALLY works

20060220
    - FIX: english spelling fix

20060219
    - NEW: line wrapping in batch list works now

20060217
    - INTERNAL: Fileformat change internal and external to allow compatibility  
                with MiniPauker
    - INTERNAL: LessonFormat now version 1.7 -> LearnedTimestamp now in
                milliseconds
    - NEW: default file format: pau.gz (xml.gz remains valid to support backward
           compatibility)
    - NEW: .pau (gunziped pau.gz) files can also be openend (as MiniPauker
           outputs .pau rather than .pau.gz)

20060117
    - NEW: splashscreen in Mustang
    - FIX: fixed some speed issues when inserting cards into huge lessons

20051211
    - FIX: English grammar fix
    - RELEASE: 1.6.1
    - FIX: checking typing errors failed when matching the case

20051210
    - RELEASE: 1.6
    - NEW: pack window after loading a lesson (reverts 20030428)

20051206
    - NEW: added Herb Martin to credits list

20051201
    - FIX: must not quote but ESCAPE search tokens!
    - FIX: use WhitespaceAnalyzer instead of StandardAnalyzer in SearchEngine

20051130
    - FIX: must quote search string tokens in SearchEngine
    - FIX: use document listeners instead of keyListeners in NewCardDialog

20051116
    - FIX: apply card side font when showing typing errors

20051110
    - NEW: added financial contributors to credits in about dialog
    - TEMP: removed temporary flip card sides button in EditCardPanel

20051108
    - NEW: continuous layout for many splitpanes
    - TEMP: temporary flip card sides button in EditCardPanel
    - FIX: do not search for similar cards when list is invisible
    - FIX: indent icon was one pixel too small

20051107
    - FIX: flipping card sides was badly broken (not in v1.5)

20051028
    - FIX: speed up edit script creation in PathNodeDiff (not in v1.5)
    - FIX: restore focus after text orientation changes (not in v1.5)

20051027
    - FIX: fixed bug in newAction (only changed local lesson reference)
           (not in v1.5)
    - NEW: diff algorithm for typing errors
    - NEW: layout of typingErrorPanel now respects splitOrientation

20051025
    - FIX: more efficient use of the card side attribute "Batch" when saving
           lessons (not in v1.5)

20051024
    - FIX: lastAccessedDirectory was not set properly when opening files

20051023
    - NEW: new file format
    - NEW: text orientation can be set per card side
    - NEW: made ring tone optional
    - INTERNAL: some findbugs fixes

20051013
    - INTERNAL: replaced own mixCards() with java.util.Collections.shuffle()

20051003
    - NEW: FileChooser localization
    - FIX: Esperanto translation update (not in v1.5)

20051001
    - NEW: full credits and link to homepage in about dialog
    - NEW: Esperanto translation

20050920
    - NEW: show next expiration date in file dialog lesson preview
    - NEW: pause learning process
    - NEW: show cancel learning warning message only if necessary

20050919
    - FIX: fixed English help TOC
    - NEW: documented search limit
    - NEW: tooltips in misc config

20050914
    - RELEASE: 1.5
    - FIX: opening CSV files via recent files menu did not work

20050906
    - INTERNAL: new search engine
    - FIX: defaulting to repeat by remembreing when inserting new cards

20050813
    - NEW: automatic word wrapping in NewCardDialog
    - FIX: set correct split orientation when calling addCardsMenuitem

20050715
    - NEW: updated documentation for NetBeans-4.1
    - FIX: renamed "back side" to "reverse side"

20050713
    - FIX: set textstyle when repeat by typing

20050603
    - FIX: BatchListModel.clear() works now with empty lists
    - INTERNAL: new (faster) PreferredSizesTableModel
    - INTERNAL: removed unnecessary tool classes

20050602
    - FIX: major speedup in BatchListModel

20050601
    - FIX: canceling FontDialog works now
    - FIX: saving lesson with adjusted frontside fonts works now

20050510
    - EXTERNAL: automatically sign jar file
    - FIX: fixed German spelling

20050507
    - NEW: added french translation by Jean Benoit

20050501
    - FIX: enabling of repeatCardsButton was broken when flipping card sides

20050325
    - NEW: card import via copy & paste

20050324
    - FIX: loading lessons from command line was broken

20050322
    - NEW: remember "Keep window open" state for NewCardDialog
    - FIX: English spelling of back_side :-)
    - INTERNAL: implemented "real" focus policy

20041223
    - FIX: flipping card sides now includes fonts, front & background colors

20041016
    - FIX: SaveAs-Icon (different from Save-Icon)

20041014
    - RELEASE: 1.4
    - NEW: Dutch translation

20040927
    - FIX: spelling fixes

20040909
    - INTERNAL: update to lucene-1.4.1

20040830
    - NEW: using preferences API
    - FIX: small fix in SettingsDialog

20040722
    - FIX: reset GUI when cancelling learning or repeating
    - FIX: state of save button was not always correct
    - INTERNAL: update to lucene-1.4-final

20040720
    - NEW: JSplitPane for card sides
    - FIX: more efficient space usage in card list

20040621
    - NEW: checking for correct java version

20040515
    - FIX: [dis/en]abling of button in OpenFileDialog

20040512
    - FIX: enable repeat button when using instant repeating
    - NEW: consistently split cards vertically in the GUI

20040502
    - NEW: refurbished NewCardDialog (vertical split)

20040403
    - NEW: updated crystal icons
    - NEW: dropped support for Java-1.3
    - NEW: added wordwrap option

20040315
    - FIX: fixed CTRL-Return focus problem

20040310
    - NEW: searching for similar cards in newcarddialog
           added Jakarta Lucene v1.3
    - FIX: some findbugs corrections

20031205
    - RELEASE: 1.3.3
    - NEW: updated to JavaHelp-2.0_01
    - FIX: fixed some GUI problems when using cards with multiple rows

20030822
    - RELEASE: 1.3.2
    - NEW: updated polish translation
    - FIX: localization of statistic panel border was missing

20030821
    - FIX: closing overwritingOK dialog did not cancel
    - FIX: Text of Cancel Learning Dialog was wrong
    - FIX: updated version number :-)
    - NEW: new icon set
    - INTERN: global question/explanation panel
    - FIX: removed rollover icons (this is non-consistent)

20030816
    - RELEASE: 1.3.1
    - FIX: scrolling in card textfields was broken
    - FIX: smaller font sizes in graphical statistic for jre 1.3
    - FIX: default font in card dialog for jre 1.3 was broken (jre-1.3 bug)

20030814
    - RELEASE: 1.3

20030810
    - FIX: first typing error highlight was broken
    - NEW: edit when typing error is displayed

20030808
    - FIX: disable CTRL-N during learning

20030721
    - NEW: case sensitiveness for input checking can be configured

20030627
    - NEW: readded wakeup ringtone because jdk-1.4.2 fixes sound
           problems under linux

20030622
    - NEW: color definition per card side

20030613
    - NEW: font definition per card side

20030508
    - INTERN: smaller minimum statistics bar size
    - FIX: set minimum size for statistics splitpane part

20030507
    - NEW: support for bidirectional learning within one lesson

20030506
    - INTERN: central program version String
    - NEW: icon and title in about dialog
    - INTERN: save pauker version number into lesson files
    - FIX: trim lesson after loading
    - FIX: reset default button after reopening NewCardDialog
    - FIX: TAB/Focus/Defaultbutton handling in NewCardDialog for OK/Cancel
    - FIX: TAB directly jumps from backside to OK-Button in NewCardDialog
    - FIX: handling of font setting changes in NewCardDialog
    - NEW: flexible size of statistics bars
    - NEW: "Add Cards" Menuitem (necessary if lower window part is folded away)
    - FIX: unified top border insets on right window side
    - FIX: unified fonts on right side title borders
    - INTERN: melted typing error dialog within main window
    - INTERN: moved flipping code into lesson

20030428
    - FIX: do not pack() after opening file
           (does confuse users more than it helps)

20030424
    - NEW: added mailing list
    - NEW: typing error dialog
    - FIX: fixed NPE when clicking OK in OpenFileDialog without selecting a file
           disabled OK button when no file is selected

20030411
    - FIX: backside of ustm learning was editable (the famous Conni-Bug)
    - FIX: more distance for search dialog

20030410
    - RELEASE: 1.2    

20030326
    - FIX: redo/undo popup: update selection while scrolling
    - FIX: special handling of double click in empty batch

20030314
    - INTERN: give summary batch program-wide the same index (-1)
    - INTERN: support for simpleDateFormat dropped
    - FIX: instant repeat failed without first batch

20030313
    - FIX: position of search dialog was only optimal at the first call
    - INTERN: lazy creation of edit dialog (speedup)

20030226
    - FIX: after undo set focus on changed side

20030225
    - FIX: use pictures instead of checkbox in list
    - FIX: use margin around textareas

20030216
    - NEW: ustm/stm times are now customizable

20030215
    - NEW: integrated about dialog
    - NEW: instant repeating

20030116
    - FIX: button dis/enabling when forgetting cards
    - NEW: batch operation for repeat by typing
    - NEW: first version of polish translation

20030115
    - FIX: alpha channel for open/exit/config icons
    - INTERN: use textareas instead of editorpanes where possible

20030112
    - NEW: list representation of batch
    - NEW: new sorting methods in batch view (dates, repeating form)
    - FIX: removed sound support because of Linux problems

20030109
    - FIX: select unlearned batch in statistic when adding cards

20030105
    - FIX: forgetting from summary failed

20021228
    - NEW: Pauker can guess import filters
    - FIX: little fixes for opening and saving dialogs

20021222
    - FIX: removed initial undo in edit dialog
    - FIX: fixed layout in new card dialog

20021218
    - RELEASE: 1.1.1

20021215
    - FIX: forgetting more than one card was buggy

20021117
    - NEW: removed flip/reset buttons from toolbar (they are too seldom used)
           unified icon for forget/reset operation on card/lesson

20021114
    - NEW: improved font selection in settings dialog
    - FIX: redo table layout after font changes

20021111
    - NEW: donor list on web page

20021107
    - FIX: calculating expiration time in summary was wrong

20021106
    - NEW: option per card: repeat by typing

20021028
    - RELEASE: 1.1
    - NEW: added a PayPal account :-)

20021018
    - INTERN: simplified JTextField initialization
              (by reading the API doc you can still discover new things :-))

20020930
    - NEW: updated to JavaHelp-1.1.3

20020926
    - NEW: sound in gui (wakeup tune)
    - NEW: search & index in help

20020922
    - FIX: do not disable paste button

20020922
    - FIX: fixed default button handling 

20020920
    - NEW: finished graphical statistic
    - FIX: ignore window closing if one of the exit dialogs is visible

20020919
    - NEW: redone dialogs (Question/Reason/Actions)

20020915
    - FIX: changed TAB usage in edit dialog
           added button for TAB inserting

20020913
    - FIX: repaint batch table after font changes

20020912
    - FIX: do not loop when searching for nonexistent string

20020911
    - FIX: enable learning after moving back cards
    - FIX: think about the people killed in the wtc ->
           cry about the 10,000 kids starving each day

20020910
    - FIX: trim lesson after moving cards back
    - FIX: make localization more general (no de_DE variant any more)

20020903
    - INTERN: separated GUI from core
    - FIX: update batch border title after batch changed
    - FIX: tooltip for search button
    - FIX: set cards as unlearned when flipping lesson

20020823
    - RELEASE: 1.0.4
    - NEW: updated help texts

20020816
    - FIX: added "setlearned(false)" when moving cards back

20020814
    - FIX: sort only when cards available

20020811
    - NEW: searching for cards implemented (first version)

20020809
    - NEW: selective encoding when loading text files
    - NEW: merging of lessons
    - FIX: RMB in Windows did not work

20020804
    - RELEASE: 1.0.3
    - FIX: corrected Germish help text

20020730
    - FIX: encoding problem of XML transformation fixed
    - NEW: distribution for jre-1.3.x with XML stuff included
            - crimson
            - jaxp-1.2 (jaxp-api.jar, xalan.jar)

20020729
    - NEW: import of some text format files

20020728
    - INTERN: removed serialization
    - NEW: highlight expiration in editcarddialog
    - NEW: file chooser with lesson preview
    - NEW: pauker icon in file chooser
    - FIX: some of the english lessons were corrupted

20020719
    - NEW: fully functional summary batch
    - NEW: mark expired cards with different color in table
    - FIX: removing multiple card intervalls was broken
    - FIX: activate randomize button after adding a card
    - FIX: activate randomize button only when there are more than one card in
           batch

20020718
    - FIX: use Collections.sort() for sorting, not a TreeMap
           (different cards with equal sides get lost otherwise)

20020716
    - NEW: got rid of sorting buttons
          (replaced with header clicking, tooltips and popup-menu)

20020715
    - NEW: set batch name in card panel title
    - NEW: keep selection when sorting or mixing cards
    - FIX: only sort when left clicking table header

20020713
    - FIX: dropped support for old file format from the file filters
    - INTERN: save batchNumber in Batch, not in BatchTableModel
    - INTERN: put expiration time into batches
    - INTERN: use expirationTime instead of batchIndex as parameter for
              EditCardDialog

20020627
    - INTERN: drop resetExpiration() as we do not save serialized lessons any
              more
    - INTERN: removed public fields from lesson, batch & card
    - INTERN: now card batch number in repeatExpiredCard() gets used

20020623
    - FIX: locale dependend sorting

20020620
    - FIX: standardized date format in xml file
    - INTERN: support for serialized object file format dropped
    - INTERN: LessonTool dropped

20020619
    - RELEASE: 1.0.2
    - NEW: standard file format changed to gzipped XML

20020613
    - NEW: cards can be edited while learning/repeating cards

20020609
    - RELEASE: 1.0.1
    - NEW: the learning system can now be used with only the keyboard

20020514
    - RELEASE: 1.0
    - NEW: help system

20020514
    - FIX: start with disabled sorting icons

20020507
    - FIX: Workaround for a table bug in Java 1.3.x

20020502
    - NEW: keyboard navigation
    - FIX: follow focus when moving cards
    - FIX: toolbar in new/edit card dialog was not bound to frontside editor
           pane

20020428
    - RELEASE: 0.9.9
    - NEW: move cards back to unlearned batch
    - FIX: editing and removing cards was buggy when summary batch was selected
    - FIX: setting focus on correct card side when editing card did not work

20020426
    - NEW: move cards up & down in batch
    - NEW: show path in "About this lesson"
    - RELEASE: 0.9.8
    - FIX: remove recent files on error
    - FIX: grey out save icon
    - FIX: no path in window title
    - NEW: icons for all menu items

20020423
    - INTERN: fast table sorting

20020419
    - INTERN: tablemodel which remebers preferred sizes (much faster!)

20020412
    - NEW: transition panel from ultra-shortterm to shortterm repeating
           with a nice pulsating brain picture :-)

20020410
    - INTERN: split card dialog into "new card" and "edit card" versions

20020406
    - NEW: put strategy preferences into settings dialog

20020301
    - NEW: show timer in window title when waiting for short term memory to
           expire

20020226
    - NEW: added "keep dialog open" checkbox to EditCardDialog

20020217
    - RELEASE: 0.9.7
    - NEW: font selection for card sides

20020215
    - NEW: timer can be hidden (it might drive people crazy)

20020113
    - RELEASE: 0.9.6
    - NEW: borders around "naked" panels

20011228
    - NEW: different strategies for putting back forgotten cards
    - NEW: summary in statistic
    - FIX: initial cursor at top in EditCardDialog

20011227
    - FIX: use choosen repeating strategy with card granularity
           (was batch granular until now)
    - FIX: scrollRectToVisible() after setRowSelectionInterval()
    - NEW: We have a homepage :-)

20011219
    - RELEASE: 0.9.5
    - FIX: jump to first batch when inserting card
    - FIX: calculate table cell sizes only when visible

20011217
    - FIX: loading large lessons is now 3 times faster
    - FIX: program position is no longer centered

20011211
    - RELEASE: 0.9.4
    - FIX: cleaned up animated icon for adding new cards

20011210
    - FIX: scroll card to visible area after edit

20011204
    - FIX: scroll to bottom after card insert

20011129
    - RELEASE: 0.9.3
    - NEW: edit buttons (cut/copy/paste/undo/redo)

20011128
    - NEW: bring window to front after timeup
    - NEW: randomize batch

20011127
    - NEW: color icons for big buttons
    - NEW: buttons for sorting cards

20011126
    - FIX: set expiration time for batches at loading time
    - NEW: big "new cards" button
    - NEW: program icon

20011122
    - FIX: trim lesson after card deleted
    - FIX: set all cards to "unlearned" when resetting lesson
    - NEW: Format of lesson changed to pure data classes
    -      ensures binary compatibility in later releases
20011121
    - NEW: different strategies for repeating expired cards