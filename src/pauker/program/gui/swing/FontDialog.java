/*
 * FontDialog.java
 *
 * Created on 13. Juni 2003, 16:10
 */

package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import pauker.program.Card;

/**
 * A dialog for changing the font of Pauker flash cards
 * @author Ronny.Standtke@gmx.net
 */
public class FontDialog extends JDialog {

    private static final Logger logger =
            Logger.getLogger(FontDialog.class.getName());
    private boolean okButtonPressed;

    /** Creates new form FontDialog
     * @param parent the parent frame
     * @param initialFont the initial font
     * @param initialForeground the initial foreground color
     * @param initialBackground the initial background color
     * @param side the card side 
     */
    public FontDialog(Frame parent, Font initialFont, Color initialForeground,
            Color initialBackground, Card.Element side) {

        super(parent, true);

        initComponents();

        // fill font family combo box
        DefaultComboBoxModel fontFamilyComboModel = new DefaultComboBoxModel();
        GraphicsEnvironment graphicsEnvironment =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        String[] fontFamilyNames = 
                graphicsEnvironment.getAvailableFontFamilyNames();
        for (String fontFamilyName : fontFamilyNames) {
            fontFamilyComboModel.addElement(fontFamilyName);
        }
        fontFamilyComboBox.setModel(fontFamilyComboModel);

        // fill font sizes combo box
        Object[] fontSizes = {
            "6", "7", "8", "9",
            "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
            "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
            "30"
        };
        DefaultComboBoxModel fontSizeComboModel = 
                new DefaultComboBoxModel(fontSizes);
        fontSizeComboBox.setModel(fontSizeComboModel);

        // set initial font values in gui
        textArea.setFont(initialFont);
        textArea.setForeground(initialForeground);
        textArea.setBackground(
                initialBackground == null ? Color.WHITE : initialBackground);
        fontFamilyComboBox.setSelectedItem(initialFont.getFamily());
        fontSizeComboBox.setSelectedItem(
                Integer.toString(initialFont.getSize()));
        boldToggleButton.setSelected(initialFont.isBold());
        italicToggleButton.setSelected(initialFont.isItalic());

        // select side
        if (side != null) {
            switch (side) {
                default:
                case FRONT_SIDE:
                    frontRadioButton.setSelected(true);
                    break;
                case REVERSE_SIDE:
                    reverseSideRadioButton.setSelected(true);
                    break;
                case BOTH_SIDES:
                    bothRadioButton.setSelected(true);
            }
        }

        // resize a bit
        Dimension size = scrollPane.getPreferredSize();
        size.height += 30;
        size.width += 50;
        scrollPane.setPreferredSize(size);
        pack();

        setLocationRelativeTo(parent);
    }

    /**
     * returns <code>true</code>, when the OK button was pressed to close the
     * dialog, <code>false</code> otherwise
     * @return <code>true</code>, when the OK button was pressed to close the
     * dialog, <code>false</code> otherwise
     */
    public boolean okPressed() {
        return okButtonPressed;
    }

    /**
     * returns the font of the text area
     * @return the font of the text area
     */
    public Font getTextAreaFont() {
        return textArea.getFont();
    }

    /**
     * returns the foreground color
     * @return the foreground color
     */
    public Color getForegroundColor() {
        return textArea.getForeground();
    }

    /**
     * returns the background color
     * @return the background color
     */
    public Color getBackgroundColor() {
        return textArea.getBackground();
    }

    /**
     * returns the selected card side
     * @return the selected card side
     */
    public Card.Element getSide() {
        if (frontRadioButton.isSelected()) {
            return Card.Element.FRONT_SIDE;
        } else if (reverseSideRadioButton.isSelected()) {
            return Card.Element.REVERSE_SIDE;
        } else {
            return Card.Element.BOTH_SIDES;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup = new javax.swing.ButtonGroup();
        fontPanel = new javax.swing.JPanel();
        fontButtonPanel = new javax.swing.JPanel();
        fontFamilyComboBox = new javax.swing.JComboBox();
        fontSizeComboBox = new javax.swing.JComboBox();
        boldToggleButton = new javax.swing.JToggleButton();
        italicToggleButton = new javax.swing.JToggleButton();
        foregroundButton = new javax.swing.JButton();
        backgroundButton = new javax.swing.JButton();
        scrollPane = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        radioButtonPanel = new javax.swing.JPanel();
        frontLabel = new javax.swing.JLabel();
        frontRadioButton = new javax.swing.JRadioButton();
        backLabel = new javax.swing.JLabel();
        reverseSideRadioButton = new javax.swing.JRadioButton();
        bothLabel = new javax.swing.JLabel();
        bothRadioButton = new javax.swing.JRadioButton();
        buttonPanel = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pauker/Strings"); // NOI18N
        setTitle(bundle.getString("Set_Font")); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        fontPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("Font"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        fontPanel.setLayout(new java.awt.GridBagLayout());

        fontButtonPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fontButtonPanel.setLayout(new java.awt.GridBagLayout());

        fontFamilyComboBox.setFont(new java.awt.Font("Dialog", 0, 10));
        fontFamilyComboBox.setName("fontFamilyComboBox"); // NOI18N
        fontFamilyComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fontFamilyComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontButtonPanel.add(fontFamilyComboBox, gridBagConstraints);

        fontSizeComboBox.setEditable(true);
        fontSizeComboBox.setFont(new java.awt.Font("Dialog", 0, 10));
        fontSizeComboBox.setName("fontSizeComboBox"); // NOI18N
        fontSizeComboBox.setPreferredSize(new java.awt.Dimension(60, 24));
        fontSizeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fontSizeComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontButtonPanel.add(fontSizeComboBox, gridBagConstraints);

        boldToggleButton.setText(bundle.getString("Bold_Mnemonic")); // NOI18N
        boldToggleButton.setToolTipText(bundle.getString("Bold")); // NOI18N
        boldToggleButton.setMargin(new java.awt.Insets(2, 0, 2, 0));
        boldToggleButton.setName("boldToggleButton"); // NOI18N
        boldToggleButton.setPreferredSize(new java.awt.Dimension(26, 26));
        boldToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boldToggleButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        fontButtonPanel.add(boldToggleButton, gridBagConstraints);

        italicToggleButton.setFont(new java.awt.Font("Dialog", 3, 12));
        italicToggleButton.setText(bundle.getString("Italic_Mnemonic")); // NOI18N
        italicToggleButton.setToolTipText(bundle.getString("Italic")); // NOI18N
        italicToggleButton.setMargin(new java.awt.Insets(2, 0, 2, 0));
        italicToggleButton.setName("italicToggleButton"); // NOI18N
        italicToggleButton.setPreferredSize(new java.awt.Dimension(26, 26));
        italicToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                italicToggleButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontButtonPanel.add(italicToggleButton, gridBagConstraints);

        foregroundButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/foreground.png"))); // NOI18N
        foregroundButton.setToolTipText(bundle.getString("Font_Color")); // NOI18N
        foregroundButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        foregroundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                foregroundButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontButtonPanel.add(foregroundButton, gridBagConstraints);

        backgroundButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/background.png"))); // NOI18N
        backgroundButton.setToolTipText(bundle.getString("Background_Color")); // NOI18N
        backgroundButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        backgroundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backgroundButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        fontButtonPanel.add(backgroundButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        fontPanel.add(fontButtonPanel, gridBagConstraints);

        textArea.setText(bundle.getString("All_Letters_Sentence")); // NOI18N
        textArea.setName("textArea"); // NOI18N
        scrollPane.setViewportView(textArea);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        fontPanel.add(scrollPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        getContentPane().add(fontPanel, gridBagConstraints);

        radioButtonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("Card_Side"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        radioButtonPanel.setLayout(new java.awt.GridBagLayout());

        frontLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/frontSide.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        radioButtonPanel.add(frontLabel, gridBagConstraints);

        buttonGroup.add(frontRadioButton);
        frontRadioButton.setFont(new java.awt.Font("Dialog", 0, 10));
        frontRadioButton.setSelected(true);
        frontRadioButton.setText(bundle.getString("Frontside")); // NOI18N
        frontRadioButton.setMargin(new java.awt.Insets(2, 2, 0, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        radioButtonPanel.add(frontRadioButton, gridBagConstraints);

        backLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/reverseSide.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        radioButtonPanel.add(backLabel, gridBagConstraints);

        buttonGroup.add(reverseSideRadioButton);
        reverseSideRadioButton.setFont(new java.awt.Font("Dialog", 0, 10));
        reverseSideRadioButton.setText(bundle.getString("Reverse_Side")); // NOI18N
        reverseSideRadioButton.setMargin(new java.awt.Insets(0, 2, 0, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        radioButtonPanel.add(reverseSideRadioButton, gridBagConstraints);

        bothLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/bothSides.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        radioButtonPanel.add(bothLabel, gridBagConstraints);

        buttonGroup.add(bothRadioButton);
        bothRadioButton.setFont(new java.awt.Font("Dialog", 0, 10));
        bothRadioButton.setText(bundle.getString("Both_Sides")); // NOI18N
        bothRadioButton.setMargin(new java.awt.Insets(0, 2, 0, 2));
        bothRadioButton.setName("bothRadioButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        radioButtonPanel.add(bothRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        getContentPane().add(radioButtonPanel, gridBagConstraints);

        buttonPanel.setLayout(new java.awt.GridBagLayout());

        okButton.setFont(new java.awt.Font("Dialog", 0, 10));
        okButton.setText(bundle.getString("OK")); // NOI18N
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        buttonPanel.add(okButton, gridBagConstraints);

        cancelButton.setFont(new java.awt.Font("Dialog", 0, 10));
        cancelButton.setText(bundle.getString("Cancel")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        buttonPanel.add(cancelButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 10, 0);
        getContentPane().add(buttonPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    private void backgroundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backgroundButtonActionPerformed
        Color background = JColorChooser.showDialog(this, 
                PaukerFrame.STRINGS.getString("Choose_Background_Color"),
                Color.white);
        if (background != null) {
            textArea.setBackground(background);
        }
    }//GEN-LAST:event_backgroundButtonActionPerformed

    private void foregroundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_foregroundButtonActionPerformed
        Color foreground = JColorChooser.showDialog(this,
                PaukerFrame.STRINGS.getString("Choose_Font_Color"),
                Color.black);
        if (foreground != null) {
            textArea.setForeground(foreground);
        }
    }//GEN-LAST:event_foregroundButtonActionPerformed

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        okButtonPressed = true;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void italicToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_italicToggleButtonActionPerformed
        setFontStyle(Font.ITALIC, italicToggleButton.isSelected());
    }//GEN-LAST:event_italicToggleButtonActionPerformed

    private void boldToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boldToggleButtonActionPerformed
        setFontStyle(Font.BOLD, boldToggleButton.isSelected());
    }//GEN-LAST:event_boldToggleButtonActionPerformed

    private void setFontStyle(int style, boolean setStyle) {
        Font oldFont = textArea.getFont();
        int newStyle = oldFont.getStyle();
        if (setStyle) {
            newStyle += style;
        } else {
            newStyle -= style;
        }
        Font newFont = oldFont.deriveFont(newStyle);
        textArea.setFont(newFont);
    }

    private void fontSizeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fontSizeComboBoxActionPerformed
        String sizeString = (String) fontSizeComboBox.getSelectedItem();
        try {
            int fontSize = Integer.parseInt(sizeString);
            Font oldFont = textArea.getFont();
            Font newFont = oldFont.deriveFont((float) fontSize);
            textArea.setFont(newFont);
        } catch (Exception e) {
            logger.log(Level.SEVERE,
                    "could not parse font size \"" + sizeString + "\"", e);
        }
    }//GEN-LAST:event_fontSizeComboBoxActionPerformed

    private void fontFamilyComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fontFamilyComboBoxActionPerformed
        Font oldFont = textArea.getFont();
        int fontSize = oldFont.getSize();
        int fontStyle = oldFont.getStyle();
        String fontFamilyName = (String) fontFamilyComboBox.getSelectedItem();
        Font font = new Font(fontFamilyName, fontStyle, fontSize);
        textArea.setFont(font);
    }//GEN-LAST:event_fontFamilyComboBoxActionPerformed

    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel backLabel;
    private javax.swing.JButton backgroundButton;
    private javax.swing.JToggleButton boldToggleButton;
    private javax.swing.JLabel bothLabel;
    private javax.swing.JRadioButton bothRadioButton;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel fontButtonPanel;
    private javax.swing.JComboBox fontFamilyComboBox;
    private javax.swing.JPanel fontPanel;
    private javax.swing.JComboBox fontSizeComboBox;
    private javax.swing.JButton foregroundButton;
    private javax.swing.JLabel frontLabel;
    private javax.swing.JRadioButton frontRadioButton;
    private javax.swing.JToggleButton italicToggleButton;
    private javax.swing.JButton okButton;
    private javax.swing.JPanel radioButtonPanel;
    private javax.swing.JRadioButton reverseSideRadioButton;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTextArea textArea;
    // End of variables declaration//GEN-END:variables
}
