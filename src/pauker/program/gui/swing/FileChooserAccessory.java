/*
 * FileChooserAccessory.java
 *
 * Created on 28. Juli 2002, 12:08
 */
package pauker.program.gui.swing;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.DateFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.table.TableColumnModel;
import pauker.program.Lesson;
import pauker.program.Pauker;

/**
 * The accessory for the Pauker file dialog (provides lesson previews)
 * @author Ronny.Standtke@gmx.net
 */
public class FileChooserAccessory
        extends JPanel
        implements PropertyChangeListener {

    private static final Logger logger = 
            Logger.getLogger(FileChooserAccessory.class.getName());
    private final ResourceBundle strings =
            ResourceBundle.getBundle("pauker/Strings");
    private final Lesson emptyLesson;
    private final DateFormat dateFormat;
    private final StatisticTableModel statisticTableModel;
    private final CardLayout cardLayout;
    private String encoding;

    /** Creates new form FileChooserAccessory */
    public FileChooserAccessory() {

        emptyLesson = new Lesson();

        dateFormat = DateFormat.getDateTimeInstance(
                DateFormat.SHORT, DateFormat.SHORT);

        initComponents();

        cardLayout = (CardLayout) getLayout();

        statisticTableModel = new StatisticTableModel(
                emptyLesson, statisticTable, new Dimension(400, 400));
        StatisticTableCellRenderer statisticTableCellRenderer = 
                new StatisticTableCellRenderer();
        TableColumnModel statisticTableColumnModel = 
                statisticTable.getColumnModel();
        statisticTableColumnModel.getColumn(0).setCellRenderer(
                statisticTableCellRenderer);
        statisticTableColumnModel.getColumn(1).setCellRenderer(
                statisticTableCellRenderer);
        statisticTableColumnModel.getColumn(2).setCellRenderer(
                statisticTableCellRenderer);

        // init with empty lesson
        privateSetLesson(emptyLesson);

        // fix preferred size
        Dimension preferredSize = getPreferredSize();
        setPreferredSize(preferredSize);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Erzeugter Quelltext ">//GEN-BEGIN:initComponents
    private void initComponents() {
        noLessonPanel = new javax.swing.JPanel();
        noLessonLabel = new javax.swing.JLabel();
        lessonPanel = new javax.swing.JPanel();
        statisticScrollPane = new javax.swing.JScrollPane();
        statisticTable = new javax.swing.JTable();
        nextExpirationLabel = new javax.swing.JLabel();
        nextExpirationTextField = new javax.swing.JTextField();

        setLayout(new java.awt.CardLayout());

        noLessonPanel.setLayout(new java.awt.GridBagLayout());

        noLessonPanel.setName("noLessonPanel");
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pauker/Strings"); // NOI18N
        noLessonLabel.setText(bundle.getString("Not_A_Pauker_Lesson")); // NOI18N
        noLessonPanel.add(noLessonLabel, new java.awt.GridBagConstraints());

        add(noLessonPanel, "noLessonPanel");

        lessonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("Statistics"))); // NOI18N
        lessonPanel.setName("lessonPanel");
        statisticTable.setRowSelectionAllowed(false);
        statisticScrollPane.setViewportView(statisticTable);

        nextExpirationLabel.setText(bundle.getString("Next_Expiration")); // NOI18N

        nextExpirationTextField.setEditable(false);

        org.jdesktop.layout.GroupLayout lessonPanelLayout = new org.jdesktop.layout.GroupLayout(lessonPanel);
        lessonPanel.setLayout(lessonPanelLayout);
        lessonPanelLayout.setHorizontalGroup(
            lessonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, lessonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(nextExpirationLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nextExpirationTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                .addContainerGap())
            .add(statisticScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
        );
        lessonPanelLayout.setVerticalGroup(
            lessonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, lessonPanelLayout.createSequentialGroup()
                .add(statisticScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(lessonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(nextExpirationLabel)
                    .add(nextExpirationTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        add(lessonPanel, "lessonPanel");

    }// </editor-fold>//GEN-END:initComponents
    /**
     * sets the lesson to preview
     * @param newLesson the lesson to preview
     */
    public void setLesson(Lesson newLesson) {
        privateSetLesson(newLesson);
    }

    /**
     * shows that the selected file is no Pauker lesson
     */
    public void setNoPaukerLessonSelected() {
        cardLayout.show(this, "noLessonPanel");
        noLessonLabel.setText(strings.getString("Not_A_Pauker_Lesson"));
    }

    /**
     * shows that no file is selected
     */
    public void setNoFileSelected() {
        cardLayout.show(this, "noLessonPanel");
        noLessonLabel.setText(strings.getString("No_File_Selected"));
    }

    /**
     * sets the encoding to use
     * @param encoding
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String property = propertyChangeEvent.getPropertyName();

        if (property.equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)) {
            try {
                File selectedFile = (File) propertyChangeEvent.getNewValue();
                if (selectedFile == null || !selectedFile.exists()) {
                    setNoFileSelected();
                } else {
                    String path = selectedFile.getPath();
                    Lesson newLesson = Pauker.openLesson(path, encoding);
                    setLesson(newLesson);
                }
            } catch (Exception exception) {
                // exception will occur when clicking on all non-Pauker-lesson
                // files (may safely ignore this exception...)
                logger.log(Level.SEVERE, null, exception);
                setNoPaukerLessonSelected();
            }
        }
    }
        
    private void privateSetLesson(Lesson newLesson) {
        cardLayout.show(this, "lessonPanel");
        newLesson.refreshExpiration();
        newLesson.trim();

        long nextExpirationDate = newLesson.getNextExpirationTime();
        if (nextExpirationDate == Long.MAX_VALUE) {
            setExpirationDateVisible(false);
        } else {
            String nextExpirationDateString = 
                    dateFormat.format(nextExpirationDate);
            nextExpirationTextField.setText(nextExpirationDateString);
            setExpirationDateVisible(true);
        }

        statisticTableModel.setLesson(newLesson);
    }

    private void setExpirationDateVisible(boolean visible) {
        nextExpirationLabel.setVisible(visible);
        nextExpirationTextField.setVisible(visible);
    }
    
    // Variablendeklaration - nicht modifizieren//GEN-BEGIN:variables
    private javax.swing.JPanel lessonPanel;
    private javax.swing.JLabel nextExpirationLabel;
    private javax.swing.JTextField nextExpirationTextField;
    private javax.swing.JLabel noLessonLabel;
    private javax.swing.JPanel noLessonPanel;
    private javax.swing.JScrollPane statisticScrollPane;
    private javax.swing.JTable statisticTable;
    // Ende der Variablendeklaration//GEN-END:variables
}
