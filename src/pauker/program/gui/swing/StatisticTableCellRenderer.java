/*
 * StatisticsTableCellRenderer.java
 *
 * Created on 15. November 2001, 20:50
 */
package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class StatisticTableCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(
                table, value, isSelected, hasFocus, row, column);

        if (isSelected) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        }

        // set background color
        if (row == 0) {
            // unlearned batch
            setBackground(PaukerFrame.RED);

        } else if (row == table.getRowCount() - 1) {
            // summary on the last line
            setBackground(Color.lightGray);

        } else {
            if (column == 2) {
                // expired long term cards
                setBackground(PaukerFrame.BLUE);
            } else {
                // non-expired long term cards
                setBackground(PaukerFrame.GREEN);
            }
        }

        return this;
    }
}
