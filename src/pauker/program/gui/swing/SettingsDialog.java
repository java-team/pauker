/*
 * FontDialog.java
 *
 * Created on 16. Februar 2002, 15:15
 */

package pauker.program.gui.swing;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.text.MessageFormat;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import pauker.program.Pauker;

/**
 * The Pauker settings dialog.
 * @author Ronny.Standtke@gmx.net
 */
public class SettingsDialog extends javax.swing.JDialog {

    private final CardLayout cardLayout;
    private int ustmTime;
    private int stmTime;
    private int searchLimit;
    private boolean okButtonPressed;

    /**
     * creates a new SettingsDialog
     * @param paukerFrame the Pauker frame
     */
    public SettingsDialog(PaukerFrame paukerFrame) {
        super(paukerFrame, true);

        initComponents();

        cardLayout = (CardLayout) cardPanel.getLayout();

        DefaultListModel menuListmodel = new DefaultListModel();
        menuListmodel.addElement(PaukerFrame.STRINGS.getString("Strategies"));
        menuListmodel.addElement(PaukerFrame.STRINGS.getString("Times"));
        menuListmodel.addElement(PaukerFrame.STRINGS.getString("Misc"));

        menuList.setModel(menuListmodel);
        menuList.setCellRenderer(new MyListCellRenderer());
        menuList.setSelectedIndex(0);

        // WORKAROUND:
        // preferred scrollpane height is much too large
        int scrollPaneHeight = menuList.getCellBounds(0, menuList.getModel().getSize() - 1).height;
        Dimension preferredScrollPaneSize = menuScrollPane.getPreferredSize();
        preferredScrollPaneSize.height = scrollPaneHeight + 10;
        menuScrollPane.setPreferredSize(preferredScrollPaneSize);
        menuScrollPane.setMinimumSize(preferredScrollPaneSize);
        // endof WORKAROUND

        // strategies
        switch (paukerFrame.getRepeatingStrategy()) {
            case OLDEST_FIRST:
                repeatOldestFirstRadioButton.setSelected(true);
                break;
            case NEWEST_FIRST:
                repeatNewestFirstRadioButton.setSelected(true);
                break;
            case RANDOM_ORDER:
                repeatRandomRadioButton.setSelected(true);
        }
        switch (paukerFrame.getPutBackStrategy()) {
            case ON_TOP:
                putBackTopRadioButton.setSelected(true);
                break;
            case AT_BOTTOM:
                putBackBottomRadioButton.setSelected(true);
                break;
            case ANYWHERE:
                putBackRandomRadioButton.setSelected(true);
        }

        // times
        ustmTextField.setText(String.valueOf(paukerFrame.getUstmTime()));
        stmTextField.setText(String.valueOf(paukerFrame.getStmTime() / 60));

        // misc
        caseCheckBox.setSelected(paukerFrame.isMatchCase());
        wordWrapCheckBox.setSelected(paukerFrame.isLineWrapping());
        searchLimitTextField.setText(String.valueOf(paukerFrame.getSearchLimit()));
        playRingToneCheckBox.setSelected(paukerFrame.isPlayRingTone());

        pack();
        setLocationRelativeTo(paukerFrame);
    }

    /**
     * returns <code>true</code>, when the OK button was pressed to close the
     * dialog, <code>false</code> otherwise
     * @return <code>true</code>, when the OK button was pressed to close the
     * dialog, <code>false</code> otherwise
     */
    public boolean okPressed() {
        return okButtonPressed;
    }

    /**
     * returns the configured repeating strategy
     * @return the configured repeating strategy
     */
    public Pauker.RepeatingStrategy getRepeatingStrategy() {
        if (repeatNewestFirstRadioButton.isSelected()) {
            return Pauker.RepeatingStrategy.NEWEST_FIRST;
        } else if (repeatOldestFirstRadioButton.isSelected()) {
            return Pauker.RepeatingStrategy.OLDEST_FIRST;
        } else {
            return Pauker.RepeatingStrategy.RANDOM_ORDER;
        }
    }

    /**
     * returns the configured putback strategy
     * @return the configured putback strategy
     */
    public Pauker.PutbackStrategy getPutBackStrategy() {
        if (putBackTopRadioButton.isSelected()) {
            return Pauker.PutbackStrategy.ON_TOP;
        } else if (putBackBottomRadioButton.isSelected()) {
            return Pauker.PutbackStrategy.AT_BOTTOM;
        } else {
            return Pauker.PutbackStrategy.ANYWHERE;
        }
    }

    /**
     * returns the configured time for the ultra short memory (in seconds)
     * @return the configured time for the ultra short memory (in seconds)
     */
    public int getUSTMTime() {
        return ustmTime;
    }

    /**
     * returns the configured time for the short memory (in seconds)
     * @return the configured time for the short memory (in seconds)
     */
    public int getSTMTime() {
        return stmTime * 60;
    }

    /**
     * returns <code>true</code>, if match case is selected, <code>false</code>
     * otherwise
     * @return <code>true</code>, if match case is selected, <code>false</code>
     * otherwise
     */
    public boolean isMatchCaseSelected() {
        return caseCheckBox.isSelected();
    }

    /**
     * returns <code>true</code>, if line wrapping is selected,
     * <code>false</code> otherwise
     * @return <code>true</code>, if line wrapping is selected,
     * <code>false</code> otherwise
     */
    public boolean isLineWrappingSelected() {
        return wordWrapCheckBox.isSelected();
    }

    /**
     * returns the minimum number of characters before searching for similar
     * cards is started
     * @return the minimum number of characters before searching for similar
     * cards is started
     */
    public int getSearchLimit() {
        return searchLimit;
    }

    /**
     * returns <code>true</code>, if playing the ringtone is selected,
     * <code>false</code> otherwise
     * @return <code>true</code>, if playing the ringtone is selected,
     * <code>false</code> otherwise
     */
    public boolean isPlayRingToneSelected() {
        return playRingToneCheckBox.isSelected();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        repeatingStrategyButtonGroup = new javax.swing.ButtonGroup();
        putBackStrategyButtonGroup = new javax.swing.ButtonGroup();
        menuScrollPane = new javax.swing.JScrollPane();
        menuList = new javax.swing.JList();
        cardPanel = new javax.swing.JPanel();
        strategyPanel = new javax.swing.JPanel();
        repeatingPanel = new javax.swing.JPanel();
        repeatOldestFirstRadioButton = new javax.swing.JRadioButton();
        repeatNewestFirstRadioButton = new javax.swing.JRadioButton();
        repeatRandomRadioButton = new javax.swing.JRadioButton();
        putBackPanel = new javax.swing.JPanel();
        putBackTopRadioButton = new javax.swing.JRadioButton();
        putBackBottomRadioButton = new javax.swing.JRadioButton();
        putBackRandomRadioButton = new javax.swing.JRadioButton();
        timePanel = new javax.swing.JPanel();
        ustmLabel = new javax.swing.JLabel();
        ustmTextField = new javax.swing.JTextField();
        ustmUnitLabel = new javax.swing.JLabel();
        stmLabel = new javax.swing.JLabel();
        stmTextField = new javax.swing.JTextField();
        stmUnitLabel = new javax.swing.JLabel();
        defaultButton = new javax.swing.JButton();
        miscPanel = new javax.swing.JPanel();
        caseCheckBox = new javax.swing.JCheckBox();
        wordWrapCheckBox = new javax.swing.JCheckBox();
        searchLimitLabel = new javax.swing.JLabel();
        searchLimitTextField = new javax.swing.JTextField();
        playRingToneCheckBox = new javax.swing.JCheckBox();
        jSeparator1 = new javax.swing.JSeparator();
        buttonPanel = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pauker/Strings"); // NOI18N
        setTitle(bundle.getString("Settings")); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        menuList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        menuList.setName("menuList"); // NOI18N
        menuList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                menuListValueChanged(evt);
            }
        });
        menuScrollPane.setViewportView(menuList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        getContentPane().add(menuScrollPane, gridBagConstraints);

        cardPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        cardPanel.setLayout(new java.awt.CardLayout());

        strategyPanel.setLayout(new java.awt.GridBagLayout());

        repeatingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("Repeating_Strategy"))); // NOI18N
        repeatingPanel.setLayout(new java.awt.GridBagLayout());

        repeatingStrategyButtonGroup.add(repeatOldestFirstRadioButton);
        repeatOldestFirstRadioButton.setText(bundle.getString("Oldest_Cards_First")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        repeatingPanel.add(repeatOldestFirstRadioButton, gridBagConstraints);

        repeatingStrategyButtonGroup.add(repeatNewestFirstRadioButton);
        repeatNewestFirstRadioButton.setText(bundle.getString("Newest_Cards_First")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        repeatingPanel.add(repeatNewestFirstRadioButton, gridBagConstraints);

        repeatingStrategyButtonGroup.add(repeatRandomRadioButton);
        repeatRandomRadioButton.setText(bundle.getString("Random_Order")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 5, 10);
        repeatingPanel.add(repeatRandomRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        strategyPanel.add(repeatingPanel, gridBagConstraints);

        putBackPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("Putback_Strategy"))); // NOI18N
        putBackPanel.setLayout(new java.awt.GridBagLayout());

        putBackStrategyButtonGroup.add(putBackTopRadioButton);
        putBackTopRadioButton.setText(bundle.getString("On_Top")); // NOI18N
        putBackTopRadioButton.setName("putBackTopRadioButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        putBackPanel.add(putBackTopRadioButton, gridBagConstraints);

        putBackStrategyButtonGroup.add(putBackBottomRadioButton);
        putBackBottomRadioButton.setText(bundle.getString("At_Bottom")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        putBackPanel.add(putBackBottomRadioButton, gridBagConstraints);

        putBackStrategyButtonGroup.add(putBackRandomRadioButton);
        putBackRandomRadioButton.setText(bundle.getString("Anywhere")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 5, 10);
        putBackPanel.add(putBackRandomRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        strategyPanel.add(putBackPanel, gridBagConstraints);

        cardPanel.add(strategyPanel, "strategyPanel");

        timePanel.setLayout(new java.awt.GridBagLayout());

        ustmLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ustmLabel.setText(bundle.getString("Ultra_Shortterm_Memory")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(15, 15, 0, 0);
        timePanel.add(ustmLabel, gridBagConstraints);

        ustmTextField.setColumns(3);
        ustmTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 0, 0);
        timePanel.add(ustmTextField, gridBagConstraints);

        ustmUnitLabel.setText(bundle.getString("Seconds")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 0, 10);
        timePanel.add(ustmUnitLabel, gridBagConstraints);

        stmLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        stmLabel.setText(bundle.getString("Shortterm_Memory")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 15, 0, 0);
        timePanel.add(stmLabel, gridBagConstraints);

        stmTextField.setColumns(3);
        stmTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        timePanel.add(stmTextField, gridBagConstraints);

        stmUnitLabel.setText(bundle.getString("Minutes")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 10);
        timePanel.add(stmUnitLabel, gridBagConstraints);

        defaultButton.setText(bundle.getString("Default_Values")); // NOI18N
        defaultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                defaultButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 15, 0, 0);
        timePanel.add(defaultButton, gridBagConstraints);

        cardPanel.add(timePanel, "timePanel");

        miscPanel.setLayout(new java.awt.GridBagLayout());

        caseCheckBox.setText(bundle.getString("Match_Case")); // NOI18N
        caseCheckBox.setToolTipText(bundle.getString("Match_Case_Tooltip")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        miscPanel.add(caseCheckBox, gridBagConstraints);

        wordWrapCheckBox.setText(bundle.getString("Use_Word_Wrapping")); // NOI18N
        wordWrapCheckBox.setToolTipText(bundle.getString("Use_Word_Wrapping_Tooltip")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        miscPanel.add(wordWrapCheckBox, gridBagConstraints);

        searchLimitLabel.setText(bundle.getString("Search_Limit")); // NOI18N
        searchLimitLabel.setToolTipText(bundle.getString("Search_Limit_Tooltip")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        miscPanel.add(searchLimitLabel, gridBagConstraints);

        searchLimitTextField.setToolTipText(bundle.getString("Search_Limit_Tooltip")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 0, 10);
        miscPanel.add(searchLimitTextField, gridBagConstraints);

        playRingToneCheckBox.setText(bundle.getString("Play_Ringtone")); // NOI18N
        playRingToneCheckBox.setToolTipText(bundle.getString("Play_Ringtone_Tooltip")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        miscPanel.add(playRingToneCheckBox, gridBagConstraints);

        cardPanel.add(miscPanel, "miscPanel");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        getContentPane().add(cardPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        getContentPane().add(jSeparator1, gridBagConstraints);

        okButton.setText(bundle.getString("OK")); // NOI18N
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(okButton);

        cancelButton.setText(bundle.getString("Cancel")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(buttonPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    private void defaultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defaultButtonActionPerformed
        ustmTextField.setText(String.valueOf(Pauker.USTM_TIME));
        stmTextField.setText(String.valueOf(Pauker.STM_TIME / 60));
    }//GEN-LAST:event_defaultButtonActionPerformed

    private void menuListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_menuListValueChanged
        int index = menuList.getSelectedIndex();
        switch (index) {
            case 0:
                cardLayout.show(cardPanel, "strategyPanel");
                break;
            case 1:
                cardLayout.show(cardPanel, "timePanel");
                break;
            case 2:
                cardLayout.show(cardPanel, "miscPanel");
        }
    }//GEN-LAST:event_menuListValueChanged

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        ustmTime = getNumber(ustmTextField, 1, "Invalid_Time");
        if (ustmTime == -1) {
            return;
        }

        stmTime = getNumber(stmTextField, 1, "Invalid_Time");
        if (stmTime == -1) {
            return;
        }

        searchLimit = getNumber(searchLimitTextField, 2, "Invalid_Number");
        if (searchLimit == -1) {
            return;
        }

        okButtonPressed = true;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private int getNumber(JTextField textField, int menuIndex, String invalidString) {
        String text = textField.getText();
        int time;
        try {
            time = Integer.parseInt(text);
        } catch (Exception e) {
            menuList.setSelectedIndex(menuIndex);
            textField.requestFocusInWindow();
            String errorMessage = MessageFormat.format(
                    PaukerFrame.STRINGS.getString(invalidString), text);
            JOptionPane.showMessageDialog(
                    this, errorMessage,
                    PaukerFrame.STRINGS.getString("Error"),
                    JOptionPane.ERROR_MESSAGE);
            return -1;
        }

        if (time < 1) {
            menuList.setSelectedIndex(menuIndex);
            textField.requestFocusInWindow();
            String errorMessage = PaukerFrame.STRINGS.getString("Number_Too_Small");
            JOptionPane.showMessageDialog(
                    this, errorMessage,
                    PaukerFrame.STRINGS.getString("Error"),
                    JOptionPane.ERROR_MESSAGE);
            return -1;
        }

        return time;
    }

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog

    private static class MyListCellRenderer extends DefaultListCellRenderer {

        private final ImageIcon strategiesIcon;
        private final ImageIcon timesIcon;
        private final ImageIcon miscIcon;

        public MyListCellRenderer() {
            strategiesIcon = new ImageIcon(getClass().getResource("/pauker/icons/strategies.png"));
            timesIcon = new ImageIcon(getClass().getResource("/pauker/icons/clock.png"));
            miscIcon = new ImageIcon(getClass().getResource("/pauker/icons/misc.png"));
        }

        @Override
        public Component getListCellRendererComponent(
                JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

            super.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus);

            switch (index) {
                case 0:
                    setIcon(strategiesIcon);
                    break;
                case 1:
                    setIcon(timesIcon);
                    break;
                case 2:
                    setIcon(miscIcon);
            }

            setHorizontalTextPosition(CENTER);
            setVerticalTextPosition(BOTTOM);

            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);

            setBorder(new EmptyBorder(12, 4, 12, 4));

            return this;
        }
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel cardPanel;
    private javax.swing.JCheckBox caseCheckBox;
    private javax.swing.JButton defaultButton;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JList menuList;
    private javax.swing.JScrollPane menuScrollPane;
    private javax.swing.JPanel miscPanel;
    private javax.swing.JButton okButton;
    private javax.swing.JCheckBox playRingToneCheckBox;
    private javax.swing.JRadioButton putBackBottomRadioButton;
    private javax.swing.JPanel putBackPanel;
    private javax.swing.JRadioButton putBackRandomRadioButton;
    private javax.swing.ButtonGroup putBackStrategyButtonGroup;
    private javax.swing.JRadioButton putBackTopRadioButton;
    private javax.swing.JRadioButton repeatNewestFirstRadioButton;
    private javax.swing.JRadioButton repeatOldestFirstRadioButton;
    private javax.swing.JRadioButton repeatRandomRadioButton;
    private javax.swing.JPanel repeatingPanel;
    private javax.swing.ButtonGroup repeatingStrategyButtonGroup;
    private javax.swing.JLabel searchLimitLabel;
    private javax.swing.JTextField searchLimitTextField;
    private javax.swing.JLabel stmLabel;
    private javax.swing.JTextField stmTextField;
    private javax.swing.JLabel stmUnitLabel;
    private javax.swing.JPanel strategyPanel;
    private javax.swing.JPanel timePanel;
    private javax.swing.JLabel ustmLabel;
    private javax.swing.JTextField ustmTextField;
    private javax.swing.JLabel ustmUnitLabel;
    private javax.swing.JCheckBox wordWrapCheckBox;
    // End of variables declaration//GEN-END:variables
}
