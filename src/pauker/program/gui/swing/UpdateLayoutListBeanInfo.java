/*
 * UpdateLayoutListBeanInfo.java
 *
 * Created on 27. Januar 2006, 15:01
 */

package pauker.program.gui.swing;

import java.beans.*;

/**
 * @author Ronny.Standtke@gmx.net
 */
public class UpdateLayoutListBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( pauker.program.gui.swing.UpdateLayoutList.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
        
        // Here you can add code for customizing the BeanDescriptor.
        
        return beanDescriptor;     }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_anchorSelectionIndex = 5;
    private static final int PROPERTY_autoscrolls = 6;
    private static final int PROPERTY_background = 7;
    private static final int PROPERTY_backgroundSet = 8;
    private static final int PROPERTY_border = 9;
    private static final int PROPERTY_bounds = 10;
    private static final int PROPERTY_cellRenderer = 11;
    private static final int PROPERTY_colorModel = 12;
    private static final int PROPERTY_component = 13;
    private static final int PROPERTY_componentCount = 14;
    private static final int PROPERTY_componentListeners = 15;
    private static final int PROPERTY_componentOrientation = 16;
    private static final int PROPERTY_componentPopupMenu = 17;
    private static final int PROPERTY_components = 18;
    private static final int PROPERTY_containerListeners = 19;
    private static final int PROPERTY_cursor = 20;
    private static final int PROPERTY_cursorSet = 21;
    private static final int PROPERTY_debugGraphicsOptions = 22;
    private static final int PROPERTY_displayable = 23;
    private static final int PROPERTY_doubleBuffered = 24;
    private static final int PROPERTY_dragEnabled = 25;
    private static final int PROPERTY_dropTarget = 26;
    private static final int PROPERTY_enabled = 27;
    private static final int PROPERTY_firstVisibleIndex = 28;
    private static final int PROPERTY_fixedCellHeight = 29;
    private static final int PROPERTY_fixedCellWidth = 30;
    private static final int PROPERTY_focusable = 31;
    private static final int PROPERTY_focusCycleRoot = 32;
    private static final int PROPERTY_focusCycleRootAncestor = 33;
    private static final int PROPERTY_focusListeners = 34;
    private static final int PROPERTY_focusOwner = 35;
    private static final int PROPERTY_focusTraversable = 36;
    private static final int PROPERTY_focusTraversalKeys = 37;
    private static final int PROPERTY_focusTraversalKeysEnabled = 38;
    private static final int PROPERTY_focusTraversalPolicy = 39;
    private static final int PROPERTY_focusTraversalPolicyProvider = 40;
    private static final int PROPERTY_focusTraversalPolicySet = 41;
    private static final int PROPERTY_font = 42;
    private static final int PROPERTY_fontSet = 43;
    private static final int PROPERTY_foreground = 44;
    private static final int PROPERTY_foregroundSet = 45;
    private static final int PROPERTY_graphics = 46;
    private static final int PROPERTY_graphicsConfiguration = 47;
    private static final int PROPERTY_height = 48;
    private static final int PROPERTY_hierarchyBoundsListeners = 49;
    private static final int PROPERTY_hierarchyListeners = 50;
    private static final int PROPERTY_ignoreRepaint = 51;
    private static final int PROPERTY_inheritsPopupMenu = 52;
    private static final int PROPERTY_inputContext = 53;
    private static final int PROPERTY_inputMap = 54;
    private static final int PROPERTY_inputMethodListeners = 55;
    private static final int PROPERTY_inputMethodRequests = 56;
    private static final int PROPERTY_inputVerifier = 57;
    private static final int PROPERTY_insets = 58;
    private static final int PROPERTY_keyListeners = 59;
    private static final int PROPERTY_lastVisibleIndex = 60;
    private static final int PROPERTY_layout = 61;
    private static final int PROPERTY_layoutOrientation = 62;
    private static final int PROPERTY_leadSelectionIndex = 63;
    private static final int PROPERTY_lightweight = 64;
    private static final int PROPERTY_lineWrapping = 65;
    private static final int PROPERTY_listData = 66;
    private static final int PROPERTY_listSelectionListeners = 67;
    private static final int PROPERTY_locale = 68;
    private static final int PROPERTY_location = 69;
    private static final int PROPERTY_locationOnScreen = 70;
    private static final int PROPERTY_managingFocus = 71;
    private static final int PROPERTY_maximumSize = 72;
    private static final int PROPERTY_maximumSizeSet = 73;
    private static final int PROPERTY_maxSelectionIndex = 74;
    private static final int PROPERTY_minimumSize = 75;
    private static final int PROPERTY_minimumSizeSet = 76;
    private static final int PROPERTY_minSelectionIndex = 77;
    private static final int PROPERTY_model = 78;
    private static final int PROPERTY_mouseListeners = 79;
    private static final int PROPERTY_mouseMotionListeners = 80;
    private static final int PROPERTY_mousePosition = 81;
    private static final int PROPERTY_mouseWheelListeners = 82;
    private static final int PROPERTY_name = 83;
    private static final int PROPERTY_nextFocusableComponent = 84;
    private static final int PROPERTY_opaque = 85;
    private static final int PROPERTY_optimizedDrawingEnabled = 86;
    private static final int PROPERTY_paintingTile = 87;
    private static final int PROPERTY_parent = 88;
    private static final int PROPERTY_peer = 89;
    private static final int PROPERTY_preferredScrollableViewportSize = 90;
    private static final int PROPERTY_preferredSize = 91;
    private static final int PROPERTY_preferredSizeSet = 92;
    private static final int PROPERTY_propertyChangeListeners = 93;
    private static final int PROPERTY_prototypeCellValue = 94;
    private static final int PROPERTY_registeredKeyStrokes = 95;
    private static final int PROPERTY_requestFocusEnabled = 96;
    private static final int PROPERTY_rootPane = 97;
    private static final int PROPERTY_scrollableTracksViewportHeight = 98;
    private static final int PROPERTY_scrollableTracksViewportWidth = 99;
    private static final int PROPERTY_selectedIndex = 100;
    private static final int PROPERTY_selectedIndices = 101;
    private static final int PROPERTY_selectedValue = 102;
    private static final int PROPERTY_selectedValues = 103;
    private static final int PROPERTY_selectionBackground = 104;
    private static final int PROPERTY_selectionEmpty = 105;
    private static final int PROPERTY_selectionForeground = 106;
    private static final int PROPERTY_selectionInterval = 107;
    private static final int PROPERTY_selectionMode = 108;
    private static final int PROPERTY_selectionModel = 109;
    private static final int PROPERTY_showing = 110;
    private static final int PROPERTY_size = 111;
    private static final int PROPERTY_toolkit = 112;
    private static final int PROPERTY_toolTipText = 113;
    private static final int PROPERTY_topLevelAncestor = 114;
    private static final int PROPERTY_transferHandler = 115;
    private static final int PROPERTY_treeLock = 116;
    private static final int PROPERTY_UI = 117;
    private static final int PROPERTY_UIClassID = 118;
    private static final int PROPERTY_valid = 119;
    private static final int PROPERTY_validateRoot = 120;
    private static final int PROPERTY_valueIsAdjusting = 121;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 122;
    private static final int PROPERTY_vetoableChangeListeners = 123;
    private static final int PROPERTY_visible = 124;
    private static final int PROPERTY_visibleRect = 125;
    private static final int PROPERTY_visibleRowCount = 126;
    private static final int PROPERTY_width = 127;
    private static final int PROPERTY_x = 128;
    private static final int PROPERTY_y = 129;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[130];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", pauker.program.gui.swing.UpdateLayoutList.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", pauker.program.gui.swing.UpdateLayoutList.class, "getActionMap", "setActionMap" ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", pauker.program.gui.swing.UpdateLayoutList.class, "getAlignmentX", "setAlignmentX" ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", pauker.program.gui.swing.UpdateLayoutList.class, "getAlignmentY", "setAlignmentY" ); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getAncestorListeners", null ); // NOI18N
            properties[PROPERTY_anchorSelectionIndex] = new PropertyDescriptor ( "anchorSelectionIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getAnchorSelectionIndex", null ); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", pauker.program.gui.swing.UpdateLayoutList.class, "getAutoscrolls", "setAutoscrolls" ); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", pauker.program.gui.swing.UpdateLayoutList.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", pauker.program.gui.swing.UpdateLayoutList.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", pauker.program.gui.swing.UpdateLayoutList.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", pauker.program.gui.swing.UpdateLayoutList.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_cellRenderer] = new PropertyDescriptor ( "cellRenderer", pauker.program.gui.swing.UpdateLayoutList.class, "getCellRenderer", "setCellRenderer" ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", pauker.program.gui.swing.UpdateLayoutList.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", pauker.program.gui.swing.UpdateLayoutList.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", pauker.program.gui.swing.UpdateLayoutList.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", pauker.program.gui.swing.UpdateLayoutList.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", pauker.program.gui.swing.UpdateLayoutList.class, "getComponentPopupMenu", "setComponentPopupMenu" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", pauker.program.gui.swing.UpdateLayoutList.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", pauker.program.gui.swing.UpdateLayoutList.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", pauker.program.gui.swing.UpdateLayoutList.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", pauker.program.gui.swing.UpdateLayoutList.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", pauker.program.gui.swing.UpdateLayoutList.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", pauker.program.gui.swing.UpdateLayoutList.class, "isDoubleBuffered", "setDoubleBuffered" ); // NOI18N
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", pauker.program.gui.swing.UpdateLayoutList.class, "getDragEnabled", "setDragEnabled" ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", pauker.program.gui.swing.UpdateLayoutList.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", pauker.program.gui.swing.UpdateLayoutList.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_firstVisibleIndex] = new PropertyDescriptor ( "firstVisibleIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getFirstVisibleIndex", null ); // NOI18N
            properties[PROPERTY_fixedCellHeight] = new PropertyDescriptor ( "fixedCellHeight", pauker.program.gui.swing.UpdateLayoutList.class, "getFixedCellHeight", "setFixedCellHeight" ); // NOI18N
            properties[PROPERTY_fixedCellWidth] = new PropertyDescriptor ( "fixedCellWidth", pauker.program.gui.swing.UpdateLayoutList.class, "getFixedCellWidth", "setFixedCellWidth" ); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", pauker.program.gui.swing.UpdateLayoutList.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", pauker.program.gui.swing.UpdateLayoutList.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", pauker.program.gui.swing.UpdateLayoutList.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", pauker.program.gui.swing.UpdateLayoutList.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", pauker.program.gui.swing.UpdateLayoutList.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", pauker.program.gui.swing.UpdateLayoutList.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", pauker.program.gui.swing.UpdateLayoutList.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", pauker.program.gui.swing.UpdateLayoutList.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", pauker.program.gui.swing.UpdateLayoutList.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", pauker.program.gui.swing.UpdateLayoutList.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", pauker.program.gui.swing.UpdateLayoutList.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", pauker.program.gui.swing.UpdateLayoutList.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", pauker.program.gui.swing.UpdateLayoutList.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", pauker.program.gui.swing.UpdateLayoutList.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", pauker.program.gui.swing.UpdateLayoutList.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", pauker.program.gui.swing.UpdateLayoutList.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", pauker.program.gui.swing.UpdateLayoutList.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", pauker.program.gui.swing.UpdateLayoutList.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", pauker.program.gui.swing.UpdateLayoutList.class, "getInheritsPopupMenu", "setInheritsPopupMenu" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", pauker.program.gui.swing.UpdateLayoutList.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", pauker.program.gui.swing.UpdateLayoutList.class, "getInputMap", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", pauker.program.gui.swing.UpdateLayoutList.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", pauker.program.gui.swing.UpdateLayoutList.class, "getInputVerifier", "setInputVerifier" ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", pauker.program.gui.swing.UpdateLayoutList.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_lastVisibleIndex] = new PropertyDescriptor ( "lastVisibleIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getLastVisibleIndex", null ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", pauker.program.gui.swing.UpdateLayoutList.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_layoutOrientation] = new PropertyDescriptor ( "layoutOrientation", pauker.program.gui.swing.UpdateLayoutList.class, "getLayoutOrientation", "setLayoutOrientation" ); // NOI18N
            properties[PROPERTY_leadSelectionIndex] = new PropertyDescriptor ( "leadSelectionIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getLeadSelectionIndex", null ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", pauker.program.gui.swing.UpdateLayoutList.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_lineWrapping] = new PropertyDescriptor ( "lineWrapping", pauker.program.gui.swing.UpdateLayoutList.class, null, "setLineWrapping" ); // NOI18N
            properties[PROPERTY_listData] = new PropertyDescriptor ( "listData", pauker.program.gui.swing.UpdateLayoutList.class, null, "setListData" ); // NOI18N
            properties[PROPERTY_listSelectionListeners] = new PropertyDescriptor ( "listSelectionListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getListSelectionListeners", null ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", pauker.program.gui.swing.UpdateLayoutList.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", pauker.program.gui.swing.UpdateLayoutList.class, "getLocation", "setLocation" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", pauker.program.gui.swing.UpdateLayoutList.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", pauker.program.gui.swing.UpdateLayoutList.class, "isManagingFocus", null ); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", pauker.program.gui.swing.UpdateLayoutList.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", pauker.program.gui.swing.UpdateLayoutList.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_maxSelectionIndex] = new PropertyDescriptor ( "maxSelectionIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getMaxSelectionIndex", null ); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", pauker.program.gui.swing.UpdateLayoutList.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", pauker.program.gui.swing.UpdateLayoutList.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_minSelectionIndex] = new PropertyDescriptor ( "minSelectionIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getMinSelectionIndex", null ); // NOI18N
            properties[PROPERTY_model] = new PropertyDescriptor ( "model", pauker.program.gui.swing.UpdateLayoutList.class, "getModel", "setModel" ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", pauker.program.gui.swing.UpdateLayoutList.class, "getMousePosition", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", pauker.program.gui.swing.UpdateLayoutList.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", pauker.program.gui.swing.UpdateLayoutList.class, "getNextFocusableComponent", "setNextFocusableComponent" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", pauker.program.gui.swing.UpdateLayoutList.class, "isOpaque", "setOpaque" ); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", pauker.program.gui.swing.UpdateLayoutList.class, "isOptimizedDrawingEnabled", null ); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", pauker.program.gui.swing.UpdateLayoutList.class, "isPaintingTile", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", pauker.program.gui.swing.UpdateLayoutList.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", pauker.program.gui.swing.UpdateLayoutList.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", pauker.program.gui.swing.UpdateLayoutList.class, "getPreferredScrollableViewportSize", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", pauker.program.gui.swing.UpdateLayoutList.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", pauker.program.gui.swing.UpdateLayoutList.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_prototypeCellValue] = new PropertyDescriptor ( "prototypeCellValue", pauker.program.gui.swing.UpdateLayoutList.class, "getPrototypeCellValue", "setPrototypeCellValue" ); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", pauker.program.gui.swing.UpdateLayoutList.class, "getRegisteredKeyStrokes", null ); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", pauker.program.gui.swing.UpdateLayoutList.class, "isRequestFocusEnabled", "setRequestFocusEnabled" ); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", pauker.program.gui.swing.UpdateLayoutList.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", pauker.program.gui.swing.UpdateLayoutList.class, "getScrollableTracksViewportHeight", null ); // NOI18N
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", pauker.program.gui.swing.UpdateLayoutList.class, "getScrollableTracksViewportWidth", null ); // NOI18N
            properties[PROPERTY_selectedIndex] = new PropertyDescriptor ( "selectedIndex", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectedIndex", "setSelectedIndex" ); // NOI18N
            properties[PROPERTY_selectedIndices] = new PropertyDescriptor ( "selectedIndices", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectedIndices", "setSelectedIndices" ); // NOI18N
            properties[PROPERTY_selectedValue] = new PropertyDescriptor ( "selectedValue", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectedValue", null ); // NOI18N
            properties[PROPERTY_selectedValues] = new PropertyDescriptor ( "selectedValues", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectedValues", null ); // NOI18N
            properties[PROPERTY_selectionBackground] = new PropertyDescriptor ( "selectionBackground", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectionBackground", "setSelectionBackground" ); // NOI18N
            properties[PROPERTY_selectionEmpty] = new PropertyDescriptor ( "selectionEmpty", pauker.program.gui.swing.UpdateLayoutList.class, "isSelectionEmpty", null ); // NOI18N
            properties[PROPERTY_selectionForeground] = new PropertyDescriptor ( "selectionForeground", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectionForeground", "setSelectionForeground" ); // NOI18N
            properties[PROPERTY_selectionInterval] = new IndexedPropertyDescriptor ( "selectionInterval", pauker.program.gui.swing.UpdateLayoutList.class, null, null, null, "setSelectionInterval" ); // NOI18N
            properties[PROPERTY_selectionMode] = new PropertyDescriptor ( "selectionMode", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectionMode", "setSelectionMode" ); // NOI18N
            properties[PROPERTY_selectionModel] = new PropertyDescriptor ( "selectionModel", pauker.program.gui.swing.UpdateLayoutList.class, "getSelectionModel", "setSelectionModel" ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", pauker.program.gui.swing.UpdateLayoutList.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", pauker.program.gui.swing.UpdateLayoutList.class, "getSize", "setSize" ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", pauker.program.gui.swing.UpdateLayoutList.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", pauker.program.gui.swing.UpdateLayoutList.class, "getToolTipText", "setToolTipText" ); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", pauker.program.gui.swing.UpdateLayoutList.class, "getTopLevelAncestor", null ); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", pauker.program.gui.swing.UpdateLayoutList.class, "getTransferHandler", "setTransferHandler" ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", pauker.program.gui.swing.UpdateLayoutList.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", pauker.program.gui.swing.UpdateLayoutList.class, "getUI", "setUI" ); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", pauker.program.gui.swing.UpdateLayoutList.class, "getUIClassID", null ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", pauker.program.gui.swing.UpdateLayoutList.class, "isValid", null ); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", pauker.program.gui.swing.UpdateLayoutList.class, "isValidateRoot", null ); // NOI18N
            properties[PROPERTY_valueIsAdjusting] = new PropertyDescriptor ( "valueIsAdjusting", pauker.program.gui.swing.UpdateLayoutList.class, "getValueIsAdjusting", "setValueIsAdjusting" ); // NOI18N
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", pauker.program.gui.swing.UpdateLayoutList.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" ); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", pauker.program.gui.swing.UpdateLayoutList.class, "getVetoableChangeListeners", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", pauker.program.gui.swing.UpdateLayoutList.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", pauker.program.gui.swing.UpdateLayoutList.class, "getVisibleRect", null ); // NOI18N
            properties[PROPERTY_visibleRowCount] = new PropertyDescriptor ( "visibleRowCount", pauker.program.gui.swing.UpdateLayoutList.class, "getVisibleRowCount", "setVisibleRowCount" ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", pauker.program.gui.swing.UpdateLayoutList.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", pauker.program.gui.swing.UpdateLayoutList.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", pauker.program.gui.swing.UpdateLayoutList.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
        
        // Here you can add code for customizing the properties array.
        
        return properties;     }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_listSelectionListener = 8;
    private static final int EVENT_mouseListener = 9;
    private static final int EVENT_mouseMotionListener = 10;
    private static final int EVENT_mouseWheelListener = 11;
    private static final int EVENT_propertyChangeListener = 12;
    private static final int EVENT_vetoableChangeListener = 13;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[14];
    
        try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" ); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_listSelectionListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "listSelectionListener", javax.swing.event.ListSelectionListener.class, new String[] {"valueChanged"}, "addListSelectionListener", "removeListSelectionListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
        
        // Here you can add code for customizing the event sets array.
        
        return eventSets;     }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_addSelectionInterval4 = 4;
    private static final int METHOD_applyComponentOrientation5 = 5;
    private static final int METHOD_areFocusTraversalKeysSet6 = 6;
    private static final int METHOD_bounds7 = 7;
    private static final int METHOD_checkImage8 = 8;
    private static final int METHOD_clearSelection9 = 9;
    private static final int METHOD_computeVisibleRect10 = 10;
    private static final int METHOD_contains11 = 11;
    private static final int METHOD_countComponents12 = 12;
    private static final int METHOD_createImage13 = 13;
    private static final int METHOD_createToolTip14 = 14;
    private static final int METHOD_createVolatileImage15 = 15;
    private static final int METHOD_deliverEvent16 = 16;
    private static final int METHOD_disable17 = 17;
    private static final int METHOD_dispatchEvent18 = 18;
    private static final int METHOD_doLayout19 = 19;
    private static final int METHOD_enable20 = 20;
    private static final int METHOD_enableInputMethods21 = 21;
    private static final int METHOD_ensureIndexIsVisible22 = 22;
    private static final int METHOD_findComponentAt23 = 23;
    private static final int METHOD_firePropertyChange24 = 24;
    private static final int METHOD_getActionForKeyStroke25 = 25;
    private static final int METHOD_getBounds26 = 26;
    private static final int METHOD_getCellBounds27 = 27;
    private static final int METHOD_getClientProperty28 = 28;
    private static final int METHOD_getComponentAt29 = 29;
    private static final int METHOD_getComponentZOrder30 = 30;
    private static final int METHOD_getConditionForKeyStroke31 = 31;
    private static final int METHOD_getDefaultLocale32 = 32;
    private static final int METHOD_getFontMetrics33 = 33;
    private static final int METHOD_getInsets34 = 34;
    private static final int METHOD_getListeners35 = 35;
    private static final int METHOD_getLocation36 = 36;
    private static final int METHOD_getMousePosition37 = 37;
    private static final int METHOD_getNextMatch38 = 38;
    private static final int METHOD_getPopupLocation39 = 39;
    private static final int METHOD_getPropertyChangeListeners40 = 40;
    private static final int METHOD_getScrollableBlockIncrement41 = 41;
    private static final int METHOD_getScrollableUnitIncrement42 = 42;
    private static final int METHOD_getSize43 = 43;
    private static final int METHOD_getToolTipLocation44 = 44;
    private static final int METHOD_getToolTipText45 = 45;
    private static final int METHOD_gotFocus46 = 46;
    private static final int METHOD_grabFocus47 = 47;
    private static final int METHOD_handleEvent48 = 48;
    private static final int METHOD_hasFocus49 = 49;
    private static final int METHOD_hide50 = 50;
    private static final int METHOD_imageUpdate51 = 51;
    private static final int METHOD_indexToLocation52 = 52;
    private static final int METHOD_insets53 = 53;
    private static final int METHOD_inside54 = 54;
    private static final int METHOD_invalidate55 = 55;
    private static final int METHOD_isAncestorOf56 = 56;
    private static final int METHOD_isFocusCycleRoot57 = 57;
    private static final int METHOD_isLightweightComponent58 = 58;
    private static final int METHOD_isSelectedIndex59 = 59;
    private static final int METHOD_keyDown60 = 60;
    private static final int METHOD_keyUp61 = 61;
    private static final int METHOD_layout62 = 62;
    private static final int METHOD_list63 = 63;
    private static final int METHOD_locate64 = 64;
    private static final int METHOD_location65 = 65;
    private static final int METHOD_locationToIndex66 = 66;
    private static final int METHOD_lostFocus67 = 67;
    private static final int METHOD_minimumSize68 = 68;
    private static final int METHOD_mouseDown69 = 69;
    private static final int METHOD_mouseDrag70 = 70;
    private static final int METHOD_mouseEnter71 = 71;
    private static final int METHOD_mouseExit72 = 72;
    private static final int METHOD_mouseMove73 = 73;
    private static final int METHOD_mouseUp74 = 74;
    private static final int METHOD_move75 = 75;
    private static final int METHOD_nextFocus76 = 76;
    private static final int METHOD_paint77 = 77;
    private static final int METHOD_paintAll78 = 78;
    private static final int METHOD_paintComponents79 = 79;
    private static final int METHOD_paintImmediately80 = 80;
    private static final int METHOD_postEvent81 = 81;
    private static final int METHOD_preferredSize82 = 82;
    private static final int METHOD_prepareImage83 = 83;
    private static final int METHOD_print84 = 84;
    private static final int METHOD_printAll85 = 85;
    private static final int METHOD_printComponents86 = 86;
    private static final int METHOD_putClientProperty87 = 87;
    private static final int METHOD_registerKeyboardAction88 = 88;
    private static final int METHOD_remove89 = 89;
    private static final int METHOD_removeAll90 = 90;
    private static final int METHOD_removeNotify91 = 91;
    private static final int METHOD_removePropertyChangeListener92 = 92;
    private static final int METHOD_removeSelectionInterval93 = 93;
    private static final int METHOD_repaint94 = 94;
    private static final int METHOD_requestDefaultFocus95 = 95;
    private static final int METHOD_requestFocus96 = 96;
    private static final int METHOD_requestFocusInWindow97 = 97;
    private static final int METHOD_resetKeyboardActions98 = 98;
    private static final int METHOD_reshape99 = 99;
    private static final int METHOD_resize100 = 100;
    private static final int METHOD_revalidate101 = 101;
    private static final int METHOD_scrollRectToVisible102 = 102;
    private static final int METHOD_setBounds103 = 103;
    private static final int METHOD_setComponentZOrder104 = 104;
    private static final int METHOD_setDefaultLocale105 = 105;
    private static final int METHOD_setListData106 = 106;
    private static final int METHOD_setSelectedValue107 = 107;
    private static final int METHOD_show108 = 108;
    private static final int METHOD_size109 = 109;
    private static final int METHOD_toString110 = 110;
    private static final int METHOD_transferFocus111 = 111;
    private static final int METHOD_transferFocusBackward112 = 112;
    private static final int METHOD_transferFocusDownCycle113 = 113;
    private static final int METHOD_transferFocusUpCycle114 = 114;
    private static final int METHOD_unregisterKeyboardAction115 = 115;
    private static final int METHOD_update116 = 116;
    private static final int METHOD_updateUI117 = 117;
    private static final int METHOD_validate118 = 118;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[119];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_addSelectionInterval4] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("addSelectionInterval", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_addSelectionInterval4].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation5] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation5].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet6] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet6].setDisplayName ( "" );
            methods[METHOD_bounds7] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds7].setDisplayName ( "" );
            methods[METHOD_checkImage8] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage8].setDisplayName ( "" );
            methods[METHOD_clearSelection9] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("clearSelection", new Class[] {})); // NOI18N
            methods[METHOD_clearSelection9].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect10] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_computeVisibleRect10].setDisplayName ( "" );
            methods[METHOD_contains11] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_contains11].setDisplayName ( "" );
            methods[METHOD_countComponents12] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents12].setDisplayName ( "" );
            methods[METHOD_createImage13] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage13].setDisplayName ( "" );
            methods[METHOD_createToolTip14] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip14].setDisplayName ( "" );
            methods[METHOD_createVolatileImage15] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_createVolatileImage15].setDisplayName ( "" );
            methods[METHOD_deliverEvent16] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent16].setDisplayName ( "" );
            methods[METHOD_disable17] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable17].setDisplayName ( "" );
            methods[METHOD_dispatchEvent18] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent18].setDisplayName ( "" );
            methods[METHOD_doLayout19] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout19].setDisplayName ( "" );
            methods[METHOD_enable20] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable20].setDisplayName ( "" );
            methods[METHOD_enableInputMethods21] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_enableInputMethods21].setDisplayName ( "" );
            methods[METHOD_ensureIndexIsVisible22] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("ensureIndexIsVisible", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_ensureIndexIsVisible22].setDisplayName ( "" );
            methods[METHOD_findComponentAt23] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_findComponentAt23].setDisplayName ( "" );
            methods[METHOD_firePropertyChange24] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE})); // NOI18N
            methods[METHOD_firePropertyChange24].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke25] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getActionForKeyStroke25].setDisplayName ( "" );
            methods[METHOD_getBounds26] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds26].setDisplayName ( "" );
            methods[METHOD_getCellBounds27] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getCellBounds", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getCellBounds27].setDisplayName ( "" );
            methods[METHOD_getClientProperty28] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_getClientProperty28].setDisplayName ( "" );
            methods[METHOD_getComponentAt29] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getComponentAt29].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder30] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_getComponentZOrder30].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke31] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getConditionForKeyStroke31].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale32] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale32].setDisplayName ( "" );
            methods[METHOD_getFontMetrics33] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics33].setDisplayName ( "" );
            methods[METHOD_getInsets34] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getInsets", new Class[] {java.awt.Insets.class})); // NOI18N
            methods[METHOD_getInsets34].setDisplayName ( "" );
            methods[METHOD_getListeners35] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners35].setDisplayName ( "" );
            methods[METHOD_getLocation36] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation36].setDisplayName ( "" );
            methods[METHOD_getMousePosition37] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_getMousePosition37].setDisplayName ( "" );
            methods[METHOD_getNextMatch38] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getNextMatch", new Class[] {java.lang.String.class, Integer.TYPE, javax.swing.text.Position.Bias.class})); // NOI18N
            methods[METHOD_getNextMatch38].setDisplayName ( "" );
            methods[METHOD_getPopupLocation39] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getPopupLocation39].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners40] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners40].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement41] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getScrollableBlockIncrement41].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement42] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getScrollableUnitIncrement42].setDisplayName ( "" );
            methods[METHOD_getSize43] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize43].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation44] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipLocation44].setDisplayName ( "" );
            methods[METHOD_getToolTipText45] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipText45].setDisplayName ( "" );
            methods[METHOD_gotFocus46] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus46].setDisplayName ( "" );
            methods[METHOD_grabFocus47] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus47].setDisplayName ( "" );
            methods[METHOD_handleEvent48] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent48].setDisplayName ( "" );
            methods[METHOD_hasFocus49] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus49].setDisplayName ( "" );
            methods[METHOD_hide50] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide50].setDisplayName ( "" );
            methods[METHOD_imageUpdate51] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_imageUpdate51].setDisplayName ( "" );
            methods[METHOD_indexToLocation52] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("indexToLocation", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_indexToLocation52].setDisplayName ( "" );
            methods[METHOD_insets53] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets53].setDisplayName ( "" );
            methods[METHOD_inside54] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_inside54].setDisplayName ( "" );
            methods[METHOD_invalidate55] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate55].setDisplayName ( "" );
            methods[METHOD_isAncestorOf56] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf56].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot57] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot57].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent58] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isLightweightComponent58].setDisplayName ( "" );
            methods[METHOD_isSelectedIndex59] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("isSelectedIndex", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_isSelectedIndex59].setDisplayName ( "" );
            methods[METHOD_keyDown60] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyDown60].setDisplayName ( "" );
            methods[METHOD_keyUp61] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyUp61].setDisplayName ( "" );
            methods[METHOD_layout62] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout62].setDisplayName ( "" );
            methods[METHOD_list63] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE})); // NOI18N
            methods[METHOD_list63].setDisplayName ( "" );
            methods[METHOD_locate64] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_locate64].setDisplayName ( "" );
            methods[METHOD_location65] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location65].setDisplayName ( "" );
            methods[METHOD_locationToIndex66] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("locationToIndex", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_locationToIndex66].setDisplayName ( "" );
            methods[METHOD_lostFocus67] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus67].setDisplayName ( "" );
            methods[METHOD_minimumSize68] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize68].setDisplayName ( "" );
            methods[METHOD_mouseDown69] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDown69].setDisplayName ( "" );
            methods[METHOD_mouseDrag70] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDrag70].setDisplayName ( "" );
            methods[METHOD_mouseEnter71] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseEnter71].setDisplayName ( "" );
            methods[METHOD_mouseExit72] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseExit72].setDisplayName ( "" );
            methods[METHOD_mouseMove73] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseMove73].setDisplayName ( "" );
            methods[METHOD_mouseUp74] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseUp74].setDisplayName ( "" );
            methods[METHOD_move75] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_move75].setDisplayName ( "" );
            methods[METHOD_nextFocus76] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus76].setDisplayName ( "" );
            methods[METHOD_paint77] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint77].setDisplayName ( "" );
            methods[METHOD_paintAll78] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll78].setDisplayName ( "" );
            methods[METHOD_paintComponents79] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents79].setDisplayName ( "" );
            methods[METHOD_paintImmediately80] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_paintImmediately80].setDisplayName ( "" );
            methods[METHOD_postEvent81] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent81].setDisplayName ( "" );
            methods[METHOD_preferredSize82] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize82].setDisplayName ( "" );
            methods[METHOD_prepareImage83] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage83].setDisplayName ( "" );
            methods[METHOD_print84] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print84].setDisplayName ( "" );
            methods[METHOD_printAll85] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll85].setDisplayName ( "" );
            methods[METHOD_printComponents86] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents86].setDisplayName ( "" );
            methods[METHOD_putClientProperty87] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_putClientProperty87].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction88] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE})); // NOI18N
            methods[METHOD_registerKeyboardAction88].setDisplayName ( "" );
            methods[METHOD_remove89] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("remove", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_remove89].setDisplayName ( "" );
            methods[METHOD_removeAll90] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll90].setDisplayName ( "" );
            methods[METHOD_removeNotify91] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify91].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener92] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener92].setDisplayName ( "" );
            methods[METHOD_removeSelectionInterval93] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("removeSelectionInterval", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_removeSelectionInterval93].setDisplayName ( "" );
            methods[METHOD_repaint94] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_repaint94].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus95] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus95].setDisplayName ( "" );
            methods[METHOD_requestFocus96] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus96].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow97] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow97].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions98] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions98].setDisplayName ( "" );
            methods[METHOD_reshape99] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_reshape99].setDisplayName ( "" );
            methods[METHOD_resize100] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_resize100].setDisplayName ( "" );
            methods[METHOD_revalidate101] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate101].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible102] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_scrollRectToVisible102].setDisplayName ( "" );
            methods[METHOD_setBounds103] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_setBounds103].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder104] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE})); // NOI18N
            methods[METHOD_setComponentZOrder104].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale105] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class})); // NOI18N
            methods[METHOD_setDefaultLocale105].setDisplayName ( "" );
            methods[METHOD_setListData106] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("setListData", new Class[] {java.lang.Object[].class})); // NOI18N
            methods[METHOD_setListData106].setDisplayName ( "" );
            methods[METHOD_setSelectedValue107] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("setSelectedValue", new Class[] {java.lang.Object.class, Boolean.TYPE})); // NOI18N
            methods[METHOD_setSelectedValue107].setDisplayName ( "" );
            methods[METHOD_show108] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show108].setDisplayName ( "" );
            methods[METHOD_size109] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size109].setDisplayName ( "" );
            methods[METHOD_toString110] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString110].setDisplayName ( "" );
            methods[METHOD_transferFocus111] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus111].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward112] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward112].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle113] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle113].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle114] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle114].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction115] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_unregisterKeyboardAction115].setDisplayName ( "" );
            methods[METHOD_update116] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update116].setDisplayName ( "" );
            methods[METHOD_updateUI117] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI117].setDisplayName ( "" );
            methods[METHOD_validate118] = new MethodDescriptor ( pauker.program.gui.swing.UpdateLayoutList.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate118].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
        
        // Here you can add code for customizing the methods array.
        
        return methods;     }//GEN-LAST:Methods
    
    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = "/pauker/icons/UpdateLayoutList.png";//GEN-BEGIN:Icons
    private static String iconNameC32 = null;
    private static String iconNameM16 = null;
    private static String iconNameM32 = null;//GEN-END:Icons
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
        return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
        return defaultEventIndex;
    }
    
    /**
     * This method returns an image object that can be used to
     * represent the bean in toolboxes, toolbars, etc.   Icon images
     * will typically be GIFs, but may in future include other formats.
     * <p>
     * Beans aren't required to provide icons and may return null from
     * this method.
     * <p>
     * There are four possible flavors of icons (16x16 color,
     * 32x32 color, 16x16 mono, 32x32 mono).  If a bean choses to only
     * support a single icon we recommend supporting 16x16 color.
     * <p>
     * We recommend that icons have a "transparent" background
     * so they can be rendered onto an existing background.
     *
     * @param  iconKind  The kind of icon requested.  This should be
     *    one of the constant values ICON_COLOR_16x16, ICON_COLOR_32x32,
     *    ICON_MONO_16x16, or ICON_MONO_32x32.
     * @return  An image object representing the requested icon.  May
     *    return null if no suitable icon is available.
     */
    public java.awt.Image getIcon(int iconKind) {
        switch ( iconKind ) {
            case ICON_COLOR_16x16:
                if ( iconNameC16 == null )
                    return null;
                else {
                    if( iconColor16 == null )
                        iconColor16 = loadImage( iconNameC16 );
                    return iconColor16;
                }
            case ICON_COLOR_32x32:
                if ( iconNameC32 == null )
                    return null;
                else {
                    if( iconColor32 == null )
                        iconColor32 = loadImage( iconNameC32 );
                    return iconColor32;
                }
            case ICON_MONO_16x16:
                if ( iconNameM16 == null )
                    return null;
                else {
                    if( iconMono16 == null )
                        iconMono16 = loadImage( iconNameM16 );
                    return iconMono16;
                }
            case ICON_MONO_32x32:
                if ( iconNameM32 == null )
                    return null;
                else {
                    if( iconMono32 == null )
                        iconMono32 = loadImage( iconNameM32 );
                    return iconMono32;
                }
            default: return null;
        }
    }
    
}

