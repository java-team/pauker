/*
 * BatchListModel.java
 *
 * Created on 6. Januar 2003, 19:06
 */
package pauker.program.gui.swing;

import javax.swing.AbstractListModel;
import pauker.program.Batch;
import pauker.program.Card;

public class BatchListModel extends AbstractListModel {

    private Batch batch;

    public void clear() {
        if (batch != null) {
            int size = batch.getNumberOfCards();
            if (size > 0) {
                batch.clear();
                fireIntervalRemoved(this, 0, size - 1);
            }
        }
    }

    public Object get(int index) {
        return batch.getCard(index);
    }

    public Object getElementAt(int index) {
        return batch.getCard(index);
    }

    public int getSize() {
        if (batch == null) {
            return 0;
        }
        return batch.getNumberOfCards();
    }

    public void remove(int index) {
        batch.removeCard(index);
        fireIntervalRemoved(this, index, index);
    }

    public void setBatch(Batch batch) {
        int oldSize = 0;
        if (this.batch != null) {
            oldSize = this.batch.getNumberOfCards();
        }
        this.batch = batch;
        int numberOfCards = batch.getNumberOfCards();

        // notify event listeners
        if (oldSize < numberOfCards) {
            // now we have more cards in the list
            fireContentsChanged(this, 0, oldSize - 1);
            fireIntervalAdded(this, oldSize, numberOfCards);
        } else if (oldSize > numberOfCards) {
            // now we have less cards in the list
            fireIntervalRemoved(this, numberOfCards, oldSize - 1);
            fireContentsChanged(this, 0, numberOfCards - 1);
        } else {
            // we still have the same number of cards in the list
            fireContentsChanged(this, 0, numberOfCards - 1);
        }
    }

    public void addCard(Card card) {
        batch.addCard(card);
        int cardIndex = batch.getNumberOfCards() - 1;
        fireIntervalAdded(this, cardIndex, cardIndex);
    }

    public void cardAdded() {
        int cardIndex = batch.getNumberOfCards() - 1;
        fireIntervalAdded(this, cardIndex, cardIndex);
    }

    public void cardsChanged(int firstIndex, int lastIndex) {
        fireContentsChanged(this, firstIndex, lastIndex);
    }

    public void cardsRemoved(int firstIndex, int lastIndex) {
        fireIntervalRemoved(this, firstIndex, lastIndex);
    }
}
