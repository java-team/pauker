/*
 * MyTableModel.java
 *
 * Created on 11. Juni 2001, 23:45
 */

package pauker.program.gui.swing;

import java.awt.Dimension;
import java.util.ResourceBundle;
import javax.swing.JTable;
import pauker.program.Lesson;
import pauker.program.LongTermBatch;
import tools.PreferredSizesTableModel;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class StatisticTableModel extends PreferredSizesTableModel {

    private Lesson lesson;
    private final ResourceBundle strings;

    /** Creates new MyTableModel
     * @param lesson the lesson to show
     * @param statisticTable the statistics table
     * @param maxDimension the maximum dimensions
     */
    public StatisticTableModel(
            Lesson lesson, JTable statisticTable, Dimension maxDimension) {
        super(statisticTable, maxDimension);

        this.lesson = lesson;

        strings = ResourceBundle.getBundle("pauker/Strings");

        addColumn(strings.getString("Status"));
        addColumn(strings.getString("Quantity"));
        addColumn(strings.getString("Expired"));

        initSizes();
    }

    /**
     * updates the expired cells
     */
    public void updateExpiredCells() {
        for (int i = 3; i < lesson.getNumberOfLongTermBatches(); i++) {
            fireTableCellUpdated(i, 2);
        }
    }

    /**
     * sets the lesson to show
     * @param lesson the lesson to show
     */
    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getRowCount() {
        if (lesson == null) {
            return 0;
        }
        // unlearned, long term batches, summary
        return lesson.getNumberOfLongTermBatches() + 2;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                // name column
                if (row == 0) {
                    // unlearned batch
                    return strings.getString("Not_learned");
                } else if (row == lesson.getNumberOfLongTermBatches() + 1) {
                    // summary
                    return strings.getString("Summary");
                } else {
                    // a long term batch
                    return strings.getString("Batch") + ' ' + row;
                }

            case 1:
                // quantity column
                if (row == 0) {
                    // unlearned batch
                    return Integer.valueOf(
                            lesson.getUnlearnedBatch().getNumberOfCards());
                } else if (row == lesson.getNumberOfLongTermBatches() + 1) {
                    // summary
                    return Integer.valueOf(lesson.getNumberOfCards());
                } else {
                    // a long term batch
                    LongTermBatch longTermBatch =
                            lesson.getLongTermBatch(row - 1);
                    return Integer.valueOf(longTermBatch.getNumberOfCards());
                }

            case 2:
                // expired column
                if (row == 0) {
                    // unlearned batch
                    return "";
                } else if (row == lesson.getNumberOfLongTermBatches() + 1) {
                    // summary
                    return Integer.valueOf(lesson.getNumberOfExpiredCards());
                } else {
                    // a long term batch
                    LongTermBatch longTermBatch =
                            lesson.getLongTermBatch(row - 1);
                    return Integer.valueOf(
                            longTermBatch.getNumberOfExpiredCards());
                }
        }

        return "xxx";
    }
}
