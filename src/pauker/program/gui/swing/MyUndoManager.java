/*
 * MyUndoManager.java
 *
 * Created on 7. März 2003, 23:45
 */
package pauker.program.gui.swing;

import java.util.ArrayList;
import java.util.List;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class MyUndoManager extends UndoManager {

    public List<String> getUndoPresentations() {
        List<String> presentationNames = new ArrayList<String>();
        for (UndoableEdit edit : edits) {
            if (edit.canUndo()) {
                presentationNames.add(edit.getPresentationName());
            }
        }
        return presentationNames;
    }

    public void undoTo(int index) {
        for (int numberOfEdits = edits.size(),  i = numberOfEdits - 1,  undoIndex = 0; i >= 0; i--) {
            UndoableEdit edit = edits.get(i);
            if (edit.canUndo()) {
                if (undoIndex == index) {
                    undoTo(edit);
                    break;
                }
                undoIndex++;
            }
        }
    }

    public List<String> getRedoPresentations() {
        List<String> presentationNames = new ArrayList<String>();
        for (UndoableEdit edit : edits) {
            if (edit.canRedo()) {
                presentationNames.add(edit.getPresentationName());
            }
        }
        return presentationNames;
    }

    public void redoTo(int index) {
        for (int i = 0,  numberOfEdits = edits.size(),  redoIndex = 0; i < numberOfEdits; i++) {
            UndoableEdit edit = edits.get(i);
            if (edit.canRedo()) {
                if (redoIndex == index) {
                    redoTo(edit);
                    break;
                }
                redoIndex++;
            }
        }
    }
}
