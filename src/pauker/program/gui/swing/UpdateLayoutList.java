/*
 * UpdateLayoutList.java
 *
 * Created on 27. Januar 2006, 14:45
 *
 */

package pauker.program.gui.swing;

import javax.swing.JList;
import javax.swing.plaf.basic.BasicListUI;

/**
 * Rather cheesy implementation of a list that wraps its text
 * Whenever the list's layout (e.g. size) changes the list's UI is
 * instructed to update its layout too.
 *
 * @author Ronny.Standtke@gmx.net
 */
public class UpdateLayoutList extends JList {
    
    private boolean lineWrapping;
    
    /** Creates a new instance of UpdateLayoutList */
    public UpdateLayoutList() {
        setUI(new UpdateLayoutListUI());
    }
    
    @Override
    public void doLayout() {
        ((UpdateLayoutListUI) getUI()).updateLayoutState();
        super.doLayout();
    }
    
    @Override
    public boolean getScrollableTracksViewportWidth() {
        return lineWrapping || 
                (getPreferredSize().width <= getParent().getWidth());
    }
    
    /**
     * sets, if line wrapping is used
     * @param lineWrapping if <code>true</code>, line wrapping is used, if
     * <code>false</code>, line wrapping is not used
     */
    public void setLineWrapping(boolean lineWrapping) {
        this.lineWrapping = lineWrapping;
        doLayout();
        getParent().doLayout();
    }
    
    /**
     * ListUI implementation that exposes the method for updating its layout
     */
    private static class UpdateLayoutListUI extends BasicListUI {
        @Override
        public void updateLayoutState() {
            super.updateLayoutState();
        }
    }
}