/*
 * Created on Jan 21, 2005
 *
 * By: Andrew "Snoop" Tayman
 * Function: Creates a Q/A window for mass importation of notecards
 *
 */

package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;

/**
 * A panel for labelling the Pauker import dialog
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class LineLabeler extends JPanel implements DocumentListener {

    private final static Logger logger =
            Logger.getLogger(LineLabeler.class.getName());
    private final static int MARGIN = 5;
    private final JTextComponent textComponent;
    private final FontMetrics fontMetrics;
    private final int labelWidth;
    private final String frontSideString;
    private final String reverseSideString;

    /**
     * creates a new LineLabeler
     * @param textComponent the text component to label
     * @param frontSideString the string to use for the front side
     * @param reverseSideString the string to use for the reverse side
     */
    public LineLabeler(JTextComponent textComponent,
            String frontSideString, String reverseSideString) {

        this.textComponent = textComponent;
        this.frontSideString = frontSideString;
        this.reverseSideString = reverseSideString;

        textComponent.getDocument().addDocumentListener(this);

        setFont(textComponent.getFont());
        setBackground(new Color(230, 163, 4));

        // determine label side with (max of question & answer mnemonic)
        fontMetrics = getFontMetrics(getFont());
        labelWidth = 2 * MARGIN + Math.max(
                fontMetrics.stringWidth(frontSideString),
                fontMetrics.stringWidth(reverseSideString));
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        // paint a label in front of every line of our reference text
        Document document = textComponent.getDocument();
        Element rootElement = document.getDefaultRootElement();
        String label = null;

        try {
            for (int i = 0, numberOfLines = rootElement.getElementCount();
                    i < numberOfLines; i++) {

                // determine label depending on line number
                if (i % 2 == 0) {
                    label = frontSideString;
                } else {
                    label = reverseSideString;
                }

                // center label horizontally
                int width = fontMetrics.stringWidth(label);
                int x = (labelWidth - width) / 2;

                // get the corresponding line from our reference text
                Element referenceLine = rootElement.getElement(i);
                // get the global offset of the first letter of the
                // referenceLine
                int startOffset = referenceLine.getStartOffset();
                // get the rectangle of the first letter
                Rectangle rectangle = textComponent.modelToView(startOffset);
                int y = 0;
                if (rectangle != null) {
                    // get the corresponding baseline
                    y = rectangle.y + fontMetrics.getAscent();
                }

                // draw label at the corresponding baseline
                graphics.drawString(label, x, y);

                if (i % 2 != 0) {
                    // paint separator line
                    int yOffset = rectangle.y + rectangle.height;
                    graphics.drawLine(0, yOffset, labelWidth, yOffset);
                }
            }
        } catch (BadLocationException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(labelWidth, textComponent.getHeight());
    }

    public void insertUpdate(DocumentEvent event) {
        repaint();
    }

    public void removeUpdate(DocumentEvent event) {
        repaint();
    }

    public void changedUpdate(DocumentEvent event) {
        repaint();
    }
}
