/*
 * CardSide.java
 *
 * Created on 12. Oktober 2005, 21:29
 *
 */

package pauker.program;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.util.LinkedList;
import java.util.List;
import pauker.program.gui.swing.PaukerFrame;

/**
 * One side (front or reverse) of a Pauker flashcard
 * @author Ronny.Standtke@gmx.net
 */
public class CardSide {

    // content
    private String text;
    // style
    private Font font;
    private Color foregroundColor;
    private Color backgroundColor;
    private ComponentOrientation orientation;
    // learning
    private boolean repeatByTyping;
    private boolean learned;
    private int longTermBatchNumber;
    private long learnedTimestamp;
    // support for search result caching (speeds up batch rendering)
    private final List<SearchHit> searchHits;

    /**
     * creates a new CardSide
     */
    public CardSide() {
        this("");
    }

    /**
     * creates a new CardSide
     * @param text the card side text
     */
    public CardSide(String text) {
        this.text = text;
        searchHits = new LinkedList<SearchHit>();
    }

    /**
     * returns the cardside text
     * @return the cardside text
     */
    public String getText() {
        return text;
    }

    /**
     * sets the cardside text
     * @param text the cardside text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * returns the explicitly set cardside font or <CODE>null</CODE>, if no font
     * was explicitly set for this card side
     * @return the explicitly set cardside font or <CODE>null</CODE>, if no font
     * was explicitly set for this card side
     */
    public Font getFont() {
        return font;
    }

    /**
     * returns the cardside font family
     * @return the cardside font family
     */
    public String getFontFamily() {
        return font == null
                ? PaukerFrame.DEFAULT_FONT.getFamily()
                : font.getFamily();
    }

    /**
     * sets the cardside font
     * @param font the cardside font
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * returns the cardside foreground color
     * @return the cardside foreground color
     */
    public Color getForegroundColor() {
        return foregroundColor;
    }

    /**
     * sets the cardside foreground color
     * @param foregroundColor the cardside foreground color
     */
    public void setForegroundColor(Color foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    /**
     * returns the cardside background color
     * @return the cardside background color
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * sets the cardside background color
     * @param backgroundColor the cardside background color
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * returns the cardside orientation
     * @return the cardside orientation
     */
    public ComponentOrientation getOrientation() {
        return orientation;
    }

    /**
     * sets the cardside orientation
     * @param orientation the cardside orientation
     */
    public void setOrientation(ComponentOrientation orientation) {
        this.orientation = orientation;
    }

    /**
     * returns the batch number this card belongs to if this card side would be
     * the frontside
     * @return the batch number this card belongs to if this card side would be
     * the frontside
     */
    public int getLongTermBatchNumber() {
        return longTermBatchNumber;
    }

    /**
     * sets the long term batch number this card belongs to if this card side
     * would be the frontside
     * @param longTermBatchNumber the long term batch number
     */
    public void setLongTermBatchNumber(int longTermBatchNumber) {
        this.longTermBatchNumber = longTermBatchNumber;
    }

    /**
     * returns the timestamp when the cardside was learned
     * @return the timestamp when the cardside was learned
     */
    public long getLearnedTimestamp() {
        return learnedTimestamp;
    }

    /**
     * sets the timestamp when the cardside was learned
     * @param learnedTimestamp the timestamp when the cardside was learned
     */
    public void setLearnedTimestamp(long learnedTimestamp) {
        this.learnedTimestamp = learnedTimestamp;
    }

    /**
     * returns if the cardside should be repeated by typing instead of
     * memorizing
     * @return <CODE>true</CODE>, if the cardside should be repeated by typing
     * instead of memorizing, <CODE>false</CODE> otherwise
     */
    public boolean isRepeatedByTyping() {
        return repeatByTyping;
    }

    /**
     * sets if the cardside should be repeated by typing instead of memorizing
     * @param repeatByTyping <CODE>true</CODE>, if the cardside should be
     * repeated by typing instead of memorizing, <CODE>false</CODE> otherwise
     */
    public void setRepeatByTyping(boolean repeatByTyping) {
        this.repeatByTyping = repeatByTyping;
    }

    /**
     * searches for a string pattern at the card side
     * @return a list with search match indices
     * @param card the card of this card side
     * @param cardSide the side of this card side
     * @param pattern the search pattern
     * @param matchCase if we must match the case
     */
    public List<SearchHit> search(Card card, Card.Element cardSide,
            String pattern, boolean matchCase) {
        searchHits.clear();
        if (pattern == null) {
            return searchHits;
        }
        String searchText = text;
        String searchPattern = pattern;
        if (!matchCase) {
            searchText = text.toLowerCase();
            searchPattern = pattern.toLowerCase();
        }
        for (int index = searchText.indexOf(searchPattern); index != -1;) {
            searchHits.add(new SearchHit(card, cardSide, index));
            index = searchText.indexOf(searchPattern, index + 1);
        }
        return searchHits;
    }

    /**
     * returns a List of search match indices
     * @return a List of search match indices
     */
    public List<SearchHit> getSearchHits() {
        return searchHits;
    }

    /**
     * cancels the search process
     */
    public void cancelSearch() {
        searchHits.clear();
    }

    /**
     * returns the size of the font that is used for this card side
     * @return the size of the font that is used for this card side
     */
    public int getFontSize() {
        if (font == null) {
            return PaukerFrame.DEFAULT_FONT.getSize();
        }
        return font.getSize();
    }

    /**
     * returns the stlye of the font that is used for this card side
     * @return the stlye of the font that is used for this card side
     */
    public int getFontStyle() {
        if (font == null) {
            return PaukerFrame.DEFAULT_FONT.getStyle();
        }
        return font.getStyle();
    }

    /**
     * indicates if this card side is learned
     * @return <CODE>true</CODE>, if the card side is learned,
     * <CODE>false</CODE> otherwise
     */
    public boolean isLearned() {
        return learned;
    }

    /**
     * sets if the card is learned
     * @param learned <CODE>true</CODE>, if the card is learned,
     * <CODE>false</CODE> otherwise
     */
    public void setLearned(boolean learned) {
        this.learned = learned;
    }
}
